package com.ait.wicket.component.chart;

import org.testng.TestNG;
import org.testng.annotations.Test;

import com.ait.date.DateUtils;

public class TrendlineUtilTest extends TestNG {

	@Test
	public void testGetTrendline() {

		// new TimeCoordinates(DateUtils.parseDate("01.01.2001"), 5.1653233),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2011"), 5.068336)
		// ,label:"Trend Veränderung der Lebenserwartung (mit 65)"},

		// new TimeCoordinates(DateUtils.parseDate("01.01.2001").getTime(),
		// 0.6116986),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2002"), 0.07600784),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2003"), -0.13922453),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2004"), 0.6844163),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2005"), 0.2895236),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2006"), 0.6526947),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2007"), 0.33669472),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2008"), 0.32314062),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2009"), -0.1858294),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2010"), 0.3847599),
		// new TimeCoordinates(DateUtils.parseDate("01.01.2011"), 0.48218966)
		// ,label:"Veränderung der Lebenserwartung (bei Geburt)"},

		// new Date("2001/01/01 12:00:00").getTime(),5.6705375),
		// new Date("2011/01/01 12:00:00").getTime(),5.3731947)
		// ,label:"Trend Veränderung der Lebenserwartung (bei Geburt)"}

		// label:"Veränderung der Lebenserwartung (mit 65)"},
		Timeline data = new Timeline("Veränderung der Lebenserwartung (mit 65)", new TimeCoordinates[] {
				new TimeCoordinates(DateUtils.parseDate("01.01.2001"), 1.8092155f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2002"), -0.7000506f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2003"), -0.37961006f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2004"), 2.5040746f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2005"), 0.8497119f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2006"), 1.7377615f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2007"), 1.2422323f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2008"), 0.8179903f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2009"), -0.10141134f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2010"), 1.3197899f),
				new TimeCoordinates(DateUtils.parseDate("01.01.2011"), 1.3026118f)

		});
		Timeline trend = TrendlineUtil.getTrendline(data);
		System.out.print("Trendline starts: " + trend.getPoints()[0].getY());
		System.out.println(" ends: " + trend.getPoints()[1].getY());


		Timeline data2 = new Timeline("Veränderung der Lebenserwartung (bei Geburt)", new TimeCoordinates[] {
				 new TimeCoordinates(DateUtils.parseDate("01.01.2002"), 0.07600784f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2003"), -0.13922453f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2004"), 0.6844163f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2005"), 0.2895236f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2006"), 0.6526947f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2007"), 0.33669472f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2008"), 0.32314062f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2009"), -0.1858294f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2010"), 0.3847599f),
				 new TimeCoordinates(DateUtils.parseDate("01.01.2011"), 0.48218966f)
				
		});
		Timeline trend2 = TrendlineUtil.getTrendline(data2);
		System.out.print("Trendline starts: " + trend2.getPoints()[0].getY());
		System.out.println(" ends: " + trend2.getPoints()[1].getY());
	}
}
