package com.ait.wicket.component.chart.option;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.testng.TestNG;
import org.testng.annotations.Test;

import com.ait.date.DateUtils;
import com.ait.wicket.component.chart.option.XaxisSettings.Mode;

public class SettingsTest extends TestNG {

	@Test
	public void testAddSomeSettings() {
		RootSettings root = new RootSettings();
		List<String> colors = Arrays
				.asList(new String[] { "red", "yellow", "green" });
		root.set(new ColorSettings().set(colors));
		root.set(new GridSettings().setMinorHorizontalLines(true));
		root.set(new GridSettings().setMinorVerticalLines(false));
		root.set(new PieChartSettings());

		String flotrString = root.toFlotrString();
		System.out.println(flotrString);
		// String expected =
		// "grid: {\n	minorVerticalLines: false,\n	minorHorizontalLines: true\n},\nline: {\npie: {\n	show: true\n},\ncolors: [\"red\",\"yellow\",\"green\"],\n";
		// Assert.assertEquals("Settings should be '" + expected + "', but was: "
		// + flotrString, expected, flotrString);
	}

	@Test
	public void testEmptySettings() {
		RootSettings root = new RootSettings();
		String flotrString = root.toFlotrString();
		Assert.assertEquals("Settings should be empty, but was: " + flotrString, 0,
				flotrString.length());
	}

	@Test
	public void testXaxisSettings() {
		RootSettings root = new RootSettings();
		root.set(new XaxisSettings().setMode(Mode.TIME));
		String flotrString = root.toFlotrString();
		System.out.println(flotrString);
		// Assert.assertEquals("Settings should be empty, but was: " + flotrString,
		// 0,
		// flotrString.length());
	}
	
	@Test
	public void testFunctionSetting() {
		RootSettings root = new RootSettings();
		root.set(new LegendSettings().setLabelContainer("meineId"));
		String flotrString = root.toFlotrString();
		System.out.println(flotrString);
		// Assert.assertEquals("Settings should be empty, but was: " + flotrString,
		// 0,
		// flotrString.length());
	}
	
	@Test
	public void testTime() {
		System.out.println(DateUtils.parseDate("2006").getTime());
		System.out.println(DateUtils.parseDate("2007").getTime());
	}

	
}
