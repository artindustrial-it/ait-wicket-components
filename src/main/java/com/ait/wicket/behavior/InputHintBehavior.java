package com.ait.wicket.behavior;

import java.util.HashSet;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.form.Form;

/**
 * Provides "hint text" inside `<input ...>` components similar to HTML5's
 * placeholder feature.
 * 
 * @version $Id$
 * @author Richard Nichols
 */
public class InputHintBehavior extends Behavior {

	private static final long serialVersionUID = 1L;

	private Set<Component> bound = new HashSet<Component>();
	private final String hintStyle;
	private final String entryStyle;
	private final Form<?> form;
	private final String hintText;

	public InputHintBehavior(Form<?> form, String hintText, String hintStyle) {
		this(form, hintText, hintStyle, "");
	}

	public InputHintBehavior(Form<?> form, String hintText, String hintStyle,
			String entryStyle) {
		this.hintStyle = hintStyle.replace("'", "\\'");
		this.entryStyle = entryStyle.replace("'", "\\'");
		this.form = form;
		this.hintText = hintText.replace("'", "\\'");
	}

	/**
	 * Override and return false to suppress static Javascript and CSS
	 * contributions. (May be desired if you are concatenating / compressing
	 * resources as part of build process)
	 * 
	 * @return
	 */
	protected boolean autoAddToHeader() {
		return true;
	}

	@Override
	public void renderHead(Component component, IHeaderResponse response) {
		super.renderHead(component, response);
		// response.render(JavaScriptReferenceHeaderItem.forReference(getInitJS()));
		// response.render(JavaScriptReferenceHeaderItem
		// .forReference(AIT_FLOTR2_CHARTS_JS));
		// response.renderJavascript(getInitJS(), null);
		// response.renderOnDomReadyJavascript(getDomJS());
	}

	// private String getInitJS() {
	// StringBuilder js = new StringBuilder();
	// for (Component com : bound) {
	// js.append("visural_inputHints['visural_ih_").append(com.getMarkupId())
	// .append("'] = new VisuralInputHint('").append(com.getMarkupId())
	// .append("','").append("','")
	// .append(hintStyle.replace("'", "\\'")).append("', '")
	// .append(entryStyle.replace("'", "\\'")).append("');");
	// }
	// return js.toString();
	// }

	@Override
	public void bind(Component component) {
		bound.add(component);
		component.setOutputMarkupId(true);
		form.add(new InputHintFormBehavior(component.getMarkupId()));
	}

	@Override
	public void onComponentTag(Component component, ComponentTag tag) {
		String focus = tag.getAttribute("onfocus");
		String blur = tag.getAttribute("onblur");

		tag.put("onfocus", "handleFocus('" + component.getMarkupId() + "', '"
				+ hintText + "', '" + hintStyle + "', '" + entryStyle + "');" + (isNotBlank(focus) ? focus : ""));
		tag.put("onblur", "handleBlur('" + component.getMarkupId() + "', '"
				+ hintText + "', '" + hintStyle + "', '" + entryStyle + "');" + (isNotBlank(blur) ? blur: ""));
		// initialize
		String cssClass = tag.getAttribute("class");
		tag.put("class", "inputHintField " + (isNotBlank(cssClass) ? cssClass : ""));
	}

	public static boolean isNotBlank(String str) {
		return !(str == null || str.trim().length() == 0);
	}
}