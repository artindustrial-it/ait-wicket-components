package com.ait.wicket.behavior;

import org.apache.wicket.behavior.AttributeAppender;

public class EvenOddBehavior extends AttributeAppender {

	public EvenOddBehavior(int index) {
		super("class", index % 2 == 0 ? "even" : "odd");
	}

}
