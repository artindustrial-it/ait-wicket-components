package com.ait.wicket.behavior;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;

import com.ait.wicket.behavior.grid.CssClassLayout;

public class CssClassAppendingBehavior extends AttributeAppender {
	private static final String SEPARATOR = " ";
	private static final String CLASS = "class";
	private static final long serialVersionUID = 1L;

	public CssClassAppendingBehavior(
			final IModel<? extends CssClassLayout> appendModel) {
		super(CLASS, appendModel, SEPARATOR);
	}

	public CssClassAppendingBehavior(
			final ListModel<? extends CssClassLayout> appendModel) {
		super(CLASS, new CssInflaterModel(appendModel), SEPARATOR);
	}

	@SuppressWarnings("rawtypes")
	public IModel getModel() {
		// TODO MM do some generics magic so we can use the line below
		// This does not work ATM because we have a ListModel, which is actually
		// an IModel<List<CssClassLayout>> !
		// public IModel<? extends CssClassLayout> getModel() {
		IModel<?> model = super.getReplaceModel();
		if (model != null && model instanceof CssInflaterModel) {
			return ((CssInflaterModel) model).getInnerModel();
		}
		return model;
	}

	private static class CssInflaterModel extends Model<String> {

		private final ListModel<? extends CssClassLayout> appendModel;

		public CssInflaterModel(
				final ListModel<? extends CssClassLayout> appendModel) {
			this.appendModel = appendModel;
		}

		public ListModel<? extends CssClassLayout> getInnerModel() {
			return appendModel;
		}

		@Override
		public String getObject() {
			StringBuilder sb = new StringBuilder();
			for (CssClassLayout item : appendModel.getObject()) {
				sb.append(item.toString());
			}
			return sb.toString();
		}
	}
}
