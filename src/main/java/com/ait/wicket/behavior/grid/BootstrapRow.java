package com.ait.wicket.behavior.grid;

import java.io.Serializable;

public class BootstrapRow extends BootstrapLayout implements Serializable {
	private static final long serialVersionUID = 1L;

	public BootstrapRow() {
		super();
	}

	@Override
	public String toBootstrapString(BootstrapVersion version) {
		if (version == BootstrapVersion.BOOTSTRAP2) {
			return "row-fluid";
		} else{
			return "row";
		}
	}
}
