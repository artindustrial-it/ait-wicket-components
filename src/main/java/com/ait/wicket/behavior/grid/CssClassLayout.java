package com.ait.wicket.behavior.grid;

import java.io.Serializable;

public class CssClassLayout implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final CssClassLayout RIGHT = new CssClassLayout("right");
	public static final CssClassLayout RED = new CssClassLayout("red");
	public static final CssClassLayout PERCENT1 = new CssClassLayout("percent1");
	public static final CssClassLayout PERCENT2 = new CssClassLayout("percent2");
	public static final CssClassLayout PUSH_1 = new CssClassLayout("push_1");
	public static final CssClassLayout PULL_5 = new CssClassLayout("pull_5");

	private final String[] cssClass;

	public CssClassLayout(final String... cssClass) {
		this.cssClass = cssClass;
	}

	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder();
		for (final String item : cssClass) {
			sb.append(item).append(" ");
		}
		return sb.toString() + getCustomAppendings();
	}

	protected String getCustomAppendings() {
		return "";
	}
}
