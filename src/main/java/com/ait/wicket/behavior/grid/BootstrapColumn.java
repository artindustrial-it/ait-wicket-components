package com.ait.wicket.behavior.grid;

import java.io.Serializable;

public class BootstrapColumn extends BootstrapLayout implements Serializable {
	private static final long serialVersionUID = 1L;
	private final Grid grid;
	private final Type type;

	public BootstrapColumn(final Grid grid) {
		this(grid, Type.TABLET);
	}

	public BootstrapColumn(final Grid grid, final Type type) {
		super();
		this.grid = grid;
		this.type = type;
	}

	@Override
	public String toBootstrapString(final BootstrapVersion version) {
		if (version == BootstrapVersion.BOOTSTRAP2) {
			return "span" + grid.toString();
		} else {
			// } else if (version == BootstrapVersion.BOOTSTRAP3) {
			return "col-" + type.toString(version) + "-" + grid.toString();
		}
	}
}
