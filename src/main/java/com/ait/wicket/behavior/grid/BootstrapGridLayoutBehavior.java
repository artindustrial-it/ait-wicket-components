package com.ait.wicket.behavior.grid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.util.value.IValueMap;

import com.ait.wicket.component.grid.Bootstrap2GridLayoutPage;
import com.ait.wicket.component.grid.Bootstrap3GridLayoutPage;

public class BootstrapGridLayoutBehavior extends Behavior {
	private static final String ATTRIBUTE_CLASS = "class";
	private final ListModel<? extends BootstrapLayout> model;

	public BootstrapGridLayoutBehavior(
			final IModel<? extends BootstrapLayout> model) {
		super();
		this.model = new ListWrapperModel(model);
	}

	public BootstrapGridLayoutBehavior(
			final ListModel<? extends BootstrapLayout> model) {
		super();
		this.model = model;
	}

	public static BootstrapGridLayoutBehavior of(
			final BootstrapLayout... bootstrapLayouts) {
		return new BootstrapGridLayoutBehavior(new ListModel<BootstrapLayout>(
				Arrays.asList(bootstrapLayouts)));
	}

	@Override
	public void onComponentTag(final Component component, final ComponentTag tag) {
		if (tag.getType() != TagType.CLOSE) {
			appendCssClass(component, tag);
		}
	}

	/**
	 * Checks the given component tag for an instance of the attribute to modify
	 * and if all criteria are met then replace the value of this attribute with
	 * the value of the contained model object.
	 *
	 * @param component
	 *            The component
	 * @param tag
	 *            The tag to replace the attribute value for
	 */
	public void appendCssClass(final Component component, final ComponentTag tag) {
		Page page = component.getPage();
		String cssClass = null;
		if (page instanceof Bootstrap2GridLayoutPage) {
			cssClass = toString(model.getObject(), BootstrapVersion.BOOTSTRAP2);
		} else if (page instanceof Bootstrap3GridLayoutPage) {
			cssClass = toString(model.getObject(), BootstrapVersion.BOOTSTRAP3);
		}
		if (isEnabled(component)) {
			final IValueMap attributes = tag.getAttributes();
			Object oldValue = attributes.get(ATTRIBUTE_CLASS);
			if (cssClass != null) {
				attributes.put(ATTRIBUTE_CLASS,
						(oldValue != null ? oldValue.toString() + " " : "")
								+ cssClass);
			}
		}
	}

	private String toString(final List<? extends BootstrapLayout> list,
			final BootstrapVersion version) {
		StringBuilder sb = new StringBuilder();
		for (BootstrapLayout item : list) {
			sb.append(item.toBootstrapString(version)).append(" ");
		}
		return sb.toString();
	}

	public static class ListWrapperModel extends ListModel<BootstrapLayout> {

		private final IModel<? extends BootstrapLayout> innerModel;

		public ListWrapperModel(final IModel<? extends BootstrapLayout> model) {
			innerModel = model;
		}

		@Override
		public List<BootstrapLayout> getObject() {
			List<BootstrapLayout> list = new ArrayList<BootstrapLayout>();
			if (innerModel.getObject() != null) {
				list.add(innerModel.getObject());
			}
			return list;
		}
	}

	public ListModel<? extends BootstrapLayout> getModel() {
		return model;
	}
}
