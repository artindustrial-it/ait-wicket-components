package com.ait.wicket.behavior.grid;

import java.io.Serializable;

public class BootstrapVisibility extends BootstrapLayout implements
		Serializable {
	private static final long serialVersionUID = 1L;
	private boolean isVisible;
	private Type type;

	/**
	 * calls {@link BootstrapVisibility#BootstrapVisibility(Type, boolean)} with
	 * true as 2nd parameter - so it will be visible only on {@link Type}
	 * 
	 * @param type
	 */
	public BootstrapVisibility(final Type type) {
		this(type, true);
	}

	/**
	 * 
	 * @param type
	 *            the {@link Type} where this visibility modifier gets a grasp
	 * @param isVisible
	 *            if true, it will be visible only on that specific {@link Type}
	 *            if false, it will be hidden only on that specific {@link Type}
	 */
	public BootstrapVisibility(final Type type, final boolean isVisible) {
		this.type = type;
		this.isVisible = isVisible;
	}

	@Override
	public String toBootstrapString(BootstrapVersion version) {
		return (isVisible ? "visible-" : "hidden-") + type.toString(version);
	}
}
