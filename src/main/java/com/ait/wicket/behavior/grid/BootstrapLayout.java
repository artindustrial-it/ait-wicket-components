package com.ait.wicket.behavior.grid;

import java.io.Serializable;

public abstract class BootstrapLayout extends CssClassLayout implements
Serializable {
	private static final long serialVersionUID = 1L;

	protected BootstrapLayout() {
		super("");
	}

	public abstract String toBootstrapString(BootstrapVersion version);

	public static enum Grid {
		GRID_1(1), GRID_2(2), GRID_3(3), GRID_4(4), GRID_5(5), GRID_6(6), GRID_7(
				7), GRID_8(8), GRID_9(9), GRID_10(10), GRID_11(11), GRID_12(12);

		private final int value;

		private Grid(final int value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return Integer.toString(value);
		}

		public static Grid fromInteger(final Integer size) {
			if (size != null) {
				for (Grid item : values()) {
					if (item.value == size) {
						return item;
					}
				}
			}
			return null;
		}
	}

	public static enum Type {
		PHONE("phone", "xs"), TABLET("tablet", "sm"), MEDIUM_DESKTOP("desktop",
				"md"), LARGE_DESKTOP("desktop", "lg");

		private final String bootstrap2value;
		private String bootstrap3value;

		private Type(final String bootstrap2value, final String bootstrap3value) {
			this.bootstrap2value = bootstrap2value;
			this.bootstrap3value = bootstrap3value;
		}

		public String toString(final BootstrapVersion version) {
			return version == BootstrapVersion.BOOTSTRAP2 ? bootstrap2value
					: bootstrap3value;
		}

		public static Type fromString(final String type) {
			if (type != null) {
				for (Type item : values()) {
					if (item.bootstrap2value.equals(type)
							|| item.bootstrap3value.equals(type)) {
						return item;
					}
				}
			}
			return null;
		}
	}
}
