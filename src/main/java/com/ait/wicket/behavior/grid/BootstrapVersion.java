package com.ait.wicket.behavior.grid;

public enum BootstrapVersion {
	BOOTSTRAP2, BOOTSTRAP3;
}
