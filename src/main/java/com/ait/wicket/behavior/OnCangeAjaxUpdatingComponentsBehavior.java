package com.ait.wicket.behavior;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;

/**
 * Updates all given components when onchange ajax event was triggered.
 * 
 * @author Manuel
 * 
 */
public class OnCangeAjaxUpdatingComponentsBehavior extends OnChangeAjaxBehavior {
	private static final long serialVersionUID = 1L;

	private final Component[] components;

	public OnCangeAjaxUpdatingComponentsBehavior(final Component... component) {
		components = component;
		for (final Component c : components) {
			c.setOutputMarkupId(true);
		}
	}

	@Override
	protected void onUpdate(final AjaxRequestTarget target) {
		for (final Component c : components) {
			target.add(c);
		}
	}
}
