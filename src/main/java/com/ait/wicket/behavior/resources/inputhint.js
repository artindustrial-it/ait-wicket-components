function handleFocus(inputId, hintText, hintStyle, entryStyle) {
	if (jQuery('#' + inputId).val() === hintText) {
		jQuery('#' + inputId).attr('style', entryStyle);
		jQuery('#' + inputId).val('');
	}
}

function handleBlur(inputId, hintText, hintStyle) {
	if (!jQuery('#' + inputId).val()) {
		jQuery('#' + inputId).attr('style', hintStyle);
		jQuery('#' + inputId).val(hintText);
	}
}

jQuery(function () {
	jQuery.find(".inputHintField").blur();
});
