package com.ait.wicket.behavior;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

public class ConfirmationBehavior extends AttributeAppender {
	private static final String SEPARATOR = "; ";
	private static final String CLASS = "onclick";

	public ConfirmationBehavior(final IModel<String> confirmLabelModel) {
		super(CLASS, new ConfirmationWrapperModel(confirmLabelModel),
				SEPARATOR);
	}

	public static class ConfirmationWrapperModel
			extends AbstractReadOnlyModel<String> {

		private final IModel<String> confirmLabelModel;

		public ConfirmationWrapperModel(
				final IModel<String> confirmLabelModel) {
			this.confirmLabelModel = confirmLabelModel;
		}

		@Override
		public String getObject() {
			return "return confirm('" + confirmLabelModel.getObject() + "')";
		}

	}
}
