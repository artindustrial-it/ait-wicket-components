package com.ait.wicket.behavior;

import org.apache.wicket.Component;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.form.Form;

public class InputHintFormBehavior extends Behavior {
  private static final long serialVersionUID = 1L;

  private final String componentMarkupId;

  public InputHintFormBehavior(String componentMarkupId) {
      this.componentMarkupId = componentMarkupId;
  }

  @Override
  public void bind(Component component) {
      if (!Form.class.isAssignableFrom(component.getClass())) {
          throw new WicketRuntimeException("This behavior must be applied to a Form, not a regular Component.");
      }
  }

  @Override
  public void onComponentTag(Component component, ComponentTag tag) {
      super.onComponentTag(component, tag);
      String onSub = null;
      if (tag.getAttribute("onsubmit") != null) {
          onSub = getIHJS()+";"+tag.getAttribute("onsubmit");
      } else {
          onSub = getIHJS();
      }
      if (!onSub.endsWith("return true;")) {
          onSub = onSub + "; return true;";
      }
      tag.put("onsubmit", onSub);
  }

  private String getIHJS() {
      return "if (jQuery('#"+componentMarkupId+"').val() === visural_inputHints['visural_ih_"+componentMarkupId+"'].hintText) { jQuery('#"+componentMarkupId+"').val(''); }";
  }
}
