package com.ait.wicket.model;

import org.apache.wicket.model.ChainingModel;

public class StringModifyingModel extends ChainingModel<String> {
	private String prefix;
	private String postfix;

	public StringModifyingModel(Object modelObject) {
		this(modelObject, null, null);
	}

	public StringModifyingModel(Object modelObject, String prefix) {
		this(modelObject, prefix, null);
	}
	
	public StringModifyingModel(Object modelObject, String prefix, String postfix) {
		super(modelObject);
		this.prefix = prefix;
		this.postfix = postfix;
	}
	
	@Override
	public String getObject() {
		String result = super.getObject();
		if (prefix != null) {
			result = prefix + result;
		}
		if (postfix != null) {
			result += postfix;
		}
		return result;
	}
	
}
