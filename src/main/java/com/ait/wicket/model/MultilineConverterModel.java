package com.ait.wicket.model;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.string.Strings;

import com.ait.wicket.component.i18n.I18nLabel;

/**
 * This Model is needed for i18n multiline labels.
 * 
 * @see I18nLabel
 * @author Manuel
 * 
 */
public class MultilineConverterModel extends Model<String> {
	private static final long serialVersionUID = 1L;
	private final IModel<String> innerModel;

	public MultilineConverterModel(final IModel<String> innerModel) {
		super();
		this.innerModel = innerModel;
	}

	@Override
	public String getObject() {
		String string;
		if (innerModel != null) {
			string = innerModel.getObject();
		} else {
			string = super.getObject();
		}

		if (string != null && containsLineBreak(string)) {
			return Strings.toMultilineMarkup(string).toString();
		}
		return string;
	}

	private boolean containsLineBreak(final String string) {
		return string.contains("\n") || string.contains("\r");
	}
}
