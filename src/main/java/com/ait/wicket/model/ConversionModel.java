package com.ait.wicket.model;

import java.io.Serializable;

import org.apache.wicket.Session;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.convert.IConverter;

public class ConversionModel<T extends Serializable>
		extends Model<Serializable> {
	private final IConverter<T> converter;
	private final IModel<T> model;

	public ConversionModel(final IModel<T> model,
			final IConverter<T> converter) {
		this.model = model;
		this.converter = converter;
	}

	@Override
	public Serializable getObject() {
		return converter.convertToString(model.getObject(),
				Session.get().getLocale());
	}

	@Override
	public void setObject(final Serializable object) {
		T o = converter.convertToObject(object.toString(),
				Session.get().getLocale());
		model.setObject(o);
	}
}
