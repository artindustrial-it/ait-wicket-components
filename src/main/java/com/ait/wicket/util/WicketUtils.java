package com.ait.wicket.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class WicketUtils {

	/**
	 * @param component
	 *            to check if a behavior is added
	 * @param clazz
	 *            of the behavior
	 * @return true if the given {@link Behavior} is already added to the given
	 *         {@link Component}
	 */
	public static boolean isBehaviorAdded(final Component component,
			final Class<? extends Behavior> clazz) {
		return getBehavior(component, clazz) != null;
	}

	/**
	 * Finds and returns a {@link Behavior}. If there are more {@link Behavior}s
	 * attached to the given component, use
	 * {@link WicketUtils#getBehaviors(Component, Class)}
	 *
	 * @param component
	 *            where the behavior is added
	 * @param clazz
	 *            of the behavior
	 * @return the {@link Behavior}
	 */
	public static <T extends Behavior> T getBehavior(final Component component,
			final Class<T> clazz) {
		List<T> behaviors = getBehaviors(component, clazz);
		return (behaviors != null && behaviors.size() > 0) ? behaviors.get(0)
				: null;
	}

	/**
	 * Finds and returns a {@link List}<{@link Behavior}>. If there is only one
	 * {@link Behavior} attached to the given component, use
	 * {@link WicketUtils#getBehavior(Component, Class)}
	 *
	 * @param component
	 *            where the behavior is added
	 * @param clazz
	 *            of the behavior
	 * @return the {@link Behavior}
	 */
	public static <T extends Behavior> List<T> getBehaviors(
			final Component component, final Class<T> clazz) {
		if (component != null) {
			List<T> list = component.getBehaviors(clazz);
			if (list != null) {
				return list;
			}
		}
		return null;
	}

	/**
	 * Converts a List
	 *
	 * @param component
	 *            where the behavior is added
	 * @param clazz
	 *            of the behavior
	 * @return the {@link Behavior}
	 */
	public static <T extends Serializable> List<IModel<T>> convertToModelList(
			final List<T> list) {
		if (list != null) {
			List<IModel<T>> result = new ArrayList<IModel<T>>(list.size());
			for (T item : list) {
				result.add(new CompoundPropertyModel<T>(item));
			}
			return result;
		}
		return null;
	}

	public static Component findChildById(final String id,
			final MarkupContainer parent) {
		for (Component child : parent.visitChildren().toList()) {
			if (child.getId().equals(id)) {
				return child;
			}
		}
		return null;
	}

	public static List<Component> findChildByClass(
			final Class<Component> clazz, final MarkupContainer parent) {
		return parent.visitChildren(clazz).toList();
	}

	public static <T extends Component> T addFor(final Component forComponent,
			final T label) {
		forComponent.setOutputMarkupId(true);
		label.add(new AttributeAppender("for", Model.of(forComponent
				.getMarkupId()), " "));
		return label;
	}
}
