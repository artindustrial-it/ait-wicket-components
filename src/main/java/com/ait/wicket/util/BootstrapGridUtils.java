package com.ait.wicket.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.util.iterator.ComponentHierarchyIterator;

import com.ait.wicket.behavior.grid.BootstrapColumn;
import com.ait.wicket.behavior.grid.BootstrapGridLayoutBehavior;
import com.ait.wicket.behavior.grid.BootstrapLayout;
import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.component.grid.Bootstrap3GridLayoutPage;

public class BootstrapGridUtils {
	public static void addGridLayoutToChildren(final MarkupContainer parent) {
		addGridLayoutToChildren(parent, (Grid) null);
	}

	public static boolean isOnBootstrapPage(final Component component) {
		return (component.getPage() instanceof Bootstrap3GridLayoutPage || component
				.getPage() instanceof Bootstrap3GridLayoutPage);
	}

	public static void addGridLayoutToChildren(final MarkupContainer parent,
			Grid size) {
		if (isOnBootstrapPage(parent)) {
			List<Component> children = fetchAllVisibleChildren(parent);
			// calculate grid for one child
			if (size == null) {
				size = Grid.fromInteger(calculateSize(children.size()));
			}
			addBootstrapBehavior(children, size);
		}
	}

	public static void addGridLayoutToChildren(final MarkupContainer parent,
			final BootstrapColumn... bootstrapColumns) {
		if (isOnBootstrapPage(parent)) {
			List<Component> children = fetchAllVisibleChildren(parent);
			addBootstrapBehavior(children, bootstrapColumns);
		}
	}

	private static void addBootstrapBehavior(final List<Component> children,
			final Grid size) {
		BootstrapColumn bootstrapColumn = new BootstrapColumn(size);
		addBootstrapBehavior(children, bootstrapColumn);
	}

	public static void addBootstrapBehavior(final List<Component> children,
			final BootstrapLayout... bootstrapColumn) {
		for (Component child : children) {

			if (WicketUtils.isBehaviorAdded(child, BootstrapGridLayoutBehavior.class)) {
				BootstrapGridLayoutBehavior behavior = WicketUtils.getBehavior(child,
						BootstrapGridLayoutBehavior.class);
				// to understand why we have to do this, see
				// BootstrapGridLayoutBehavior.getModel()
				ListModel<? extends BootstrapLayout> listModel = behavior.getModel();
				List<BootstrapLayout> list = new ArrayList<BootstrapLayout>(
						listModel.getObject());
				list.addAll(Arrays.asList(bootstrapColumn));
				// TODO MM check if that is needed!
				// listModel.setObject(list);
			} else {
				child.add(BootstrapGridLayoutBehavior.of(bootstrapColumn));
			}
		}
	}

	/**
	 * fetches all visible direct children of a component
	 * 
	 * @param parent
	 * @return list of child components
	 */
	private static List<Component> fetchAllVisibleChildren(
			final MarkupContainer parent) {
		ComponentHierarchyIterator visitChildren = parent.visitChildren();
		List<Component> children = new ArrayList<Component>();
		while (visitChildren.hasNext()) {
			Component child = visitChildren.next();
			if (child.isVisible() && child.getParent() == parent) {
				children.add(child);
			}
		}
		return children;
	}

	private static int calculateSize(final int size) {
		return 12 / size;
	}

}
