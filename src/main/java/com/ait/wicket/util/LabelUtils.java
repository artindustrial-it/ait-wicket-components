package com.ait.wicket.util;

public class LabelUtils {
	public enum ReplaceString {
		BORDER, TOGGLE, LABEL;
	}

	public static String getLabelId(final String id,
			final ReplaceString... replace) {
		String result = id;

		for (ReplaceString item : replace) {
			result = result.replace(item.toString().toLowerCase(), "");
		}

		return ReplaceString.LABEL.toString().toLowerCase() + result;
	}

	public static String getLabelIdForBorder(final String id) {
		return LabelUtils.getLabelId(id, ReplaceString.BORDER);
	}

	public static String getLabelIdForToggle(final String id) {
		return LabelUtils.getLabelId(id, ReplaceString.TOGGLE);
	}
}
