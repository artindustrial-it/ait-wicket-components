package com.ait.wicket.converter;

import java.util.Locale;

import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;

import com.ait.converter.DualConverter;

public class ConverterDelegater<T> implements IConverter<T> {

	private DualConverter<T, ?> c;

	public ConverterDelegater(DualConverter<T, ?> c) {
		this.c = c;
	}
	
	public T convertToObject(String value, Locale locale)
			throws ConversionException {
		return c.convertFromHumanReadable(value);
	}

	public String convertToString(T value, Locale locale) {
		return c.convertToHumanReadable(value);
	}

}
