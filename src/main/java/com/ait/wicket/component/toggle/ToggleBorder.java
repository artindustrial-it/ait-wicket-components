package com.ait.wicket.component.toggle;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.PackageResourceReference;

import com.ait.wicket.component.i18n.I18nLabel;
import com.ait.wicket.util.LabelUtils;

/**
 * The {@link ToggleBorder} creates an open / close (aka toggle) link as Title. <br/>
 * This component is very helpful in structuring e.g. big form pages.<br/>
 * It is possible to add some custom header components or customize the images. 
 * 
 * @see Border
 * @author Manuel
 */
public class ToggleBorder extends Border {

	private final static class LinkExtension extends AjaxFallbackLink<Void> {
		private static final long serialVersionUID = 1L;
		private final IModel<Boolean> bodyVisible;

		private LinkExtension(final String id, final IModel<Boolean> bodyVisible) {
			super(id);
			this.bodyVisible = bodyVisible;
		}

		@Override
		public void onClick(final AjaxRequestTarget target) {
			bodyVisible.setObject(!bodyVisible.getObject());
			if (target != null) {
				target.add(findParent(ToggleBorder.class));
			}
		}
	}

	private static final long serialVersionUID = 1L;
	public static final String HEADER_COMPONENT_ID = "headerComponent";
	private final IModel<String> headerKeyModel;
	private final IModel<Boolean> bodyVisibleModel;
	private final Component headerComponent;
	private WebMarkupContainer header;

	// TODO MM Es sollte möglich sein, einen Toggler an ein <wicket:container>
	// tag anzuhängen, derzeit zerstört dies die
	// Funktionalität und das Aussehen -> refactor me

	/**
	 * Creates a new {@link ToggleBorder} Component. The headerKey is the id with
	 * "label" prefixed. Calles {@link ToggleBorder#ToggleBorder(String, boolean)}
	 * with true as it shall be open as default.
	 * 
	 * @param id
	 *          the wicketId
	 */
	public ToggleBorder(final String id) {
		this(id, true);
	}

	/**
	 * Creates a new {@link ToggleBorder} Component. The headerKey is the id with
	 * "label" prefixed. Calles
	 * {@link ToggleBorder#ToggleBorder(String, IModel, boolean)}.
	 * 
	 * @param id
	 *          the wicketId
	 * @param bodyVisible
	 *          if the toggler should be opened on first request
	 */
	public ToggleBorder(final String id, final boolean bodyVisible) {
		this(id, Model.of(bodyVisible));
	}

	/**
	 * Creates a new {@link ToggleBorder} Component. The headerKey is the id with
	 * "label" prefixed. Calles
	 * {@link ToggleBorder#ToggleBorder(String, IModel, Component, IModel)}.
	 * 
	 * @param id
	 *          the wicketId
	 * @param bodyVisible
	 *          if the toggler should be opened on first request
	 */
	public ToggleBorder(final String id, final IModel<Boolean> bodyVisible) {
		this(id, Model.of(LabelUtils.getLabelIdForToggle(id)), null, bodyVisible);
	}

	// /**
	// * Creates a new {@link ToggleBorder} Component. Calles
	// * {@link ToggleBorder#ToggleBorder(String, IModel, Component, boolean)}
	// with true as it shall be open as default.
	// *
	// * @param id
	// * the wicketId
	// * @param headerKeyModel
	// * a model with the key to the message resource (see {@link I18nLabel})
	// */
	// public ToggleBorder(final String id, final IModel<String> headerKeyModel)
	// {
	// this(id, headerKeyModel, true);
	// }

	/**
	 * Creates a new {@link ToggleBorder} Component. Calles
	 * {@link ToggleBorder#ToggleBorder(String, IModel, Component, boolean)} with
	 * null as Component headerComponent.
	 * 
	 * @param id
	 *          the wicketId
	 * @param headerKeyModel
	 *          a model with the key to the message resource (see
	 *          {@link I18nLabel})
	 * @param bodyVisible
	 *          if the body should be initially visible or not
	 */
	public ToggleBorder(final String id, final IModel<String> headerKeyModel,
			final boolean bodyVisible) {
		this(id, headerKeyModel, null, bodyVisible);
	}

	/**
	 * Creates a new {@link ToggleBorder} Component. Calles
	 * {@link ToggleBorder#ToggleBorder(String, IModel, Component, boolean)} with
	 * null as IModel headerKeyModel.
	 * 
	 * @param id
	 *          the wicketId
	 * @param headerComponent
	 *          a component which has to have the wicketId
	 *          {@link ToggleBorder#HEADER_COMPONENT_ID}, and which is printed in
	 *          the header / as header
	 * @param bodyVisible
	 *          if the body should be initially visible or not
	 */
	public ToggleBorder(final String id, final Component headerComponent,
			final boolean bodyVisible) {
		this(id, null, headerComponent, bodyVisible);
	}

	/**
	 * Creates a new {@link ToggleBorder} Component.
	 * 
	 * @param id
	 *          the wicketId
	 * @param headerKeyModel
	 *          a model with the key to the message resource (see
	 *          {@link I18nLabel})
	 * @param headerComponent
	 *          a component which has to have the wicketId
	 *          {@link ToggleBorder#HEADER_COMPONENT_ID}, and which is printed in
	 *          the header / as header
	 * @param bodyVisible
	 *          if the body should be initially visible or not
	 */
	public ToggleBorder(final String id, final IModel<String> headerKeyModel,
			final Component headerComponent, final boolean bodyVisible) {
		this(id, headerKeyModel, headerComponent, Model.of(bodyVisible));
	}

	/**
	 * Creates a new {@link ToggleBorder} Component.
	 * 
	 * @param id
	 *          the wicketId
	 * @param headerKeyModel
	 *          a model with the key to the message resource (see
	 *          {@link I18nLabel})
	 * @param headerComponent
	 *          a component which has to have the wicketId
	 *          {@link ToggleBorder#HEADER_COMPONENT_ID}, and which is printed in
	 *          the header / as header
	 * @param bodyVisible
	 *          if the body should be initially visible or not
	 */
	public ToggleBorder(final String id, final IModel<String> headerKeyModel,
			final Component headerComponent, final IModel<Boolean> bodyVisible) {
		super(id);
		this.headerKeyModel = headerKeyModel;
		if (headerComponent == null) {
			this.headerComponent = new EmptyPanel(HEADER_COMPONENT_ID)
					.setVisible(false);
		} else {
			this.headerComponent = headerComponent;
		}
		bodyVisibleModel = bodyVisible;
	}

	@Override
	public void renderHead(final IHeaderResponse response) {
		response.render(CssHeaderItem.forReference(new PackageResourceReference(
				this.getClass(), "resources/" + this.getClass().getSimpleName()
						+ ".css")));
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		setOutputMarkupId(true);
		add(new AttributeAppender("class", Model.of("toggle"), " "));
		if (!HEADER_COMPONENT_ID.equals(headerComponent.getId())) {
			throw new IllegalArgumentException("The Id of the header component of "
					+ this.getClass().getSimpleName() + " MUST be " + HEADER_COMPONENT_ID);
		}
		header = new WebMarkupContainer("header");
		header.add(new AttributeAppender("class",
				newToggleCssModel(bodyVisibleModel), " "));
		addToBorder(header);
		final Link<Void> link = new LinkExtension("headerLink", bodyVisibleModel);
		header.add(link);
		link.add(headerComponent);
		link.add(new I18nLabel("headerLabel", headerKeyModel));
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		getBodyContainer().setVisible(bodyVisibleModel.getObject());
	}

	public Model<String> newToggleCssModel(IModel<Boolean> openCloseModel) {
		return new ToggleModel(openCloseModel);
	}
	
	public void setBodyVisible(final boolean isVisible) {
		bodyVisibleModel.setObject(isVisible);
	}

	private static final class ToggleModel extends Model<String> {
		private static final long serialVersionUID = 1L;
		private final IModel<Boolean> innerModel;

		public ToggleModel(final IModel<Boolean> innerModel) {
			this.innerModel = innerModel;
		}

		@Override
		public String getObject() {
			if (innerModel.getObject()) {
				return "open";
			}
			return "close";
		}
	}
}
