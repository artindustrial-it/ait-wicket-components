package com.ait.wicket.component.anchor;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import com.ait.string.EmailLinkProtectorHelper;
import com.ait.wicket.component.generics.TypedPanel;

public class SecureEmailLinkPanel extends TypedPanel<String> {
	public static final JavaScriptResourceReference SECURE_JS = new JavaScriptResourceReference(
			SecureEmailLinkPanel.class, "/resources/secure-email.js");
	private final IModel<String> labelModel;
	private AttributeAppender dataEncaAppender;

	/**
	 * This prints a link to an email address and encodes it if email address
	 * encryption is enabled in the theme.
	 *
	 * So the output will be one of the two following examples:
	 *
	 * <pre>
	 * {@code
	 * <!-- example with theme email address protection enabled -->
	 * <a data-enca="href" href="em52eWdiOnpuYWhyeS56bmF1bmVnQG5lZ3ZhcWhmZ2V2bnkucGJ6">
	 * 	manuel PUNKT manhart KLAMMERAFFE artindustrial PUNKT com
	 * </a>
	 *
	 * <!-- example with theme email address protection disabled -->
	 * <a data-enca="href" href="mailto:manuel.manhart@artindustrial.com">
	 * 	manuel.manhart@artindustrial.com
	 * </a>
	 * }
	 *
	 * </pre>
	 *
	 * @param id
	 * @param mailAddressModel
	 */
	public SecureEmailLinkPanel(final String id,
			final IModel<String> mailAddressModel) {
		this(id, mailAddressModel, mailAddressModel);
	}

	/**
	 * This constructor takes a customized label for the link.
	 *
	 * @see SecureEmailLinkPanel#SecureEmailLinkPanel(String, IModel)
	 * @param id
	 * @param mailAddressModel
	 * @param labelModel
	 */
	public SecureEmailLinkPanel(final String id,
			final IModel<String> mailAddressModel,
			final IModel<String> labelModel) {
		super(id);
		setModel(new SecureEmailLinkModel(mailAddressModel,
				isLinkSecurityEnabled(mailAddressModel)));
		this.labelModel = new SecureEmailTextModel(labelModel,
				isLabelSecurityEnabled(mailAddressModel, labelModel));
	}

	protected boolean isLinkSecurityEnabled(final Object mailAddressModel) {
		return isSecurityEnabled() && !SecureEmailLinkModel.class
				.equals(mailAddressModel.getClass());
	}

	protected boolean isSecurityEnabled() {
		return true;
	}

	protected boolean isLabelSecurityEnabled(
			final IModel<String> mailAddressModel,
			final IModel<String> labelModel) {
		return isSecurityEnabled()
				&& !SecureEmailTextModel.class.equals(labelModel.getClass())
				&& mailAddressModel == labelModel;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		final WebMarkupContainer linkContainer = new WebMarkupContainer("link");
		add(linkContainer);
		linkContainer.add(new AttributeAppender("href", getModel()));
		dataEncaAppender = new AttributeAppender("data-enca", "href") {
			@Override
			public boolean isEnabled(final Component component) {
				return isSecurityEnabled();
			}
		};
		linkContainer.add(dataEncaAppender);
		linkContainer.add(new Label("label", labelModel));
	}

	@Override
	public void renderHead(final IHeaderResponse response) {
		response.render(JavaScriptHeaderItem.forReference(SECURE_JS));
	}

	public static class SecureEmailLinkModel extends Model<String> {
		private final IModel<String> innerModel;
		private final boolean encrypt;

		public SecureEmailLinkModel(final IModel<String> innerModel,
				final boolean encrypt) {
			super();
			this.innerModel = innerModel;
			this.encrypt = encrypt;
		}

		@Override
		public String getObject() {
			String result = innerModel.getObject();
			if (!EmailLinkProtectorHelper.isHrefEmailLink(result)) {
				result = "mailto:" + result;
			}
			if (encrypt) {
				result = EmailLinkProtectorHelper
						.encodeEmailHrefAttributeContent(result);
			}
			return result;
		}
	}

	public static class SecureEmailTextModel extends Model<String> {
		private final IModel<String> innerModel;
		private final boolean encrypt;

		public SecureEmailTextModel(final IModel<String> innerModel,
				final boolean encrypt) {
			super();
			this.innerModel = innerModel;
			this.encrypt = encrypt;
		}

		@Override
		public String getObject() {
			String result = innerModel.getObject();
			if (encrypt) {
				result = EmailLinkProtectorHelper
						.replaceEmailAddressInLinkText(result);
			}
			return result;
		}
	}
}
