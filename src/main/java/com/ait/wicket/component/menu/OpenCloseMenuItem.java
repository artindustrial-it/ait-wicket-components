package com.ait.wicket.component.menu;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.markup.html.WebPage;

public class OpenCloseMenuItem extends
		MenuItem<Void> implements Serializable {
	private static final long serialVersionUID = 1L;
	public Class<? extends WebPage> page;

	public OpenCloseMenuItem(final String name) {
		this(name, false);
	}

	public OpenCloseMenuItem(final String name, final boolean isOpen) {
		super(name, isOpen);
	}

	public OpenCloseMenuItem(final String name,
			final Class<? extends WebPage> page, final boolean isOpen,
			final List<MenuItem<?>> submenu) {
		super(name, isOpen, submenu);
		this.page = page;
	}
	
	@Override
	public Void getLink() {
		return null;
	}
}
