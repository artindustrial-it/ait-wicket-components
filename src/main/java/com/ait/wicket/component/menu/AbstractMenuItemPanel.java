package com.ait.wicket.component.menu;

import java.util.List;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.PackageResourceReference;

import com.ait.wicket.component.generics.TypedPanel;
import com.ait.wicket.component.i18n.I18nLabel;

public abstract class AbstractMenuItemPanel<T extends MenuItem<?>> extends TypedPanel<T> {

	private static final long serialVersionUID = 1L;
	private AbstractLink link;
	private MenuPanel submenu;
	private PropertyModel<List<MenuItem<?>>> submenuModel;
	private Behavior[] subMenuItemBehavior;

	public AbstractMenuItemPanel(final String id, final IModel<T> model) {
		super(id, new CompoundPropertyModel<T>(model));
	}

	public AbstractMenuItemPanel(final String id, final IModel<T> model,
			final Behavior[] subMenuItemBehavior) {
		this(id, model);
		this.subMenuItemBehavior = subMenuItemBehavior;
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		final MenuItem<?> menuItem = getModelObject();
		link = createLink("link");
		addOrReplace(link);
		link.add(new I18nLabel("propertyKey", "menu."
				+ menuItem.getPropertyKey()));
		submenuModel = new PropertyModel<List<MenuItem<?>>>(getModel(),
				"submenu");
		submenu = new MenuPanel("submenu", submenuModel);
		if (subMenuItemBehavior != null) {
			submenu.add(subMenuItemBehavior);
		}
		add(submenu);
		add(new Image("image", new PackageResourceReference(this.getClass(),
				"resources/" + getImageName())));
	}

	protected abstract AbstractLink createLink(String string);

	protected String getImageName() {
		final CssClassEnum cssClass = getCssClass();
		switch (cssClass) {
		case OPEN:
			return "open.png";
		case ACTIVE:
			return "active.png";
		default:
		case CLOSED:
			return "closed.png";
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isSelected() {
		// TODO MM refactor muss eine rekursive Funktion werden, da es aktuell
		// nur bis zur 1. Submenüebene korrekt
		// funktioniert
		final MenuItem<?> menuItem = getModelObject();
		if (menuItem instanceof BookmarkablePageMenuItem) {
			final BookmarkablePageMenuItem bookmarkablePageMenuItem = (BookmarkablePageMenuItem) menuItem;
			if (getPage() instanceof WebPage) {
				final Class<? extends WebPage> class1 = (Class<? extends WebPage>) getPage()
						.getClass();
				return bookmarkablePageMenuItem
						.isThisOrAnyInSubmenuSelected(class1);
			}
		}
		return false;
	}

	protected CssClassEnum getCssClass() {
		if (isSelected()) {
			final MenuItem<?> menuItem = getModelObject();
			if (menuItem.hasSubmenu()) {
				return CssClassEnum.OPEN;
			}
			return CssClassEnum.ACTIVE;
		} else {
			return CssClassEnum.CLOSED;
		}
	}

	@Override
	protected void onConfigure() {
		final MenuItem<?> menuItem = getModelObject();
		submenu.setVisible(isSubmenuVisible(menuItem));
		if (menuItem.isDisabled()) {
			link.setEnabled(false);
		}
		add(new AttributeAppender("class", Model.of(getCssClass()), " "));
	}

	protected boolean isSubmenuVisible(MenuItem<?> menuItem) {
		return menuItem.hasSubmenu() && isSelected();
	}

	protected enum CssClassEnum {
		OPEN, CLOSED, ACTIVE;

		@Override
		public String toString() {
			return super.toString().toLowerCase();
		};
	}
}
