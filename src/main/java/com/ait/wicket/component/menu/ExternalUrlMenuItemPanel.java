package com.ait.wicket.component.menu;

import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.IModel;

public class ExternalUrlMenuItemPanel extends AbstractMenuItemPanel<ExternalUrlMenuItem> {

	private static final long serialVersionUID = 1L;

	public ExternalUrlMenuItemPanel(final String id, final IModel<ExternalUrlMenuItem> model) {
		super(id, model);
	}

	public ExternalUrlMenuItemPanel(final String id, final IModel<ExternalUrlMenuItem> model,
			final Behavior[] subMenuItemBehavior) {
		super(id, model, subMenuItemBehavior);
	}

	@Override
	protected AbstractLink createLink(String id) {
		super.onInitialize();
		final ExternalUrlMenuItem menuItem = getModelObject();
		if (menuItem.getLink() != null) {
			return new ExternalLink(id,
					menuItem.getLink().toString());
		} else {
			ExternalLink link = new ExternalLink(id, "#");
			link.setEnabled(false);
			return link;
		}
	}
}
