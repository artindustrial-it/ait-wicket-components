package com.ait.wicket.component.menu;

import java.net.URL;

public class ExternalUrlMenuItem extends MenuItem<URL> {
	private static final long serialVersionUID = 1L;

	public URL url;

	public ExternalUrlMenuItem(final String name, final URL url, final boolean isSelected) {
		super(name, isSelected);
		this.url = url;
	}

	@Override
	public URL getLink() {
		return url;
	}

	public void setLink(final URL url) {
		this.url = url;
	}

	@Override
	public boolean isSelected(final URL object) {
		return false;
	}
}
