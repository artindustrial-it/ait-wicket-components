package com.ait.wicket.component.menu;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.markup.html.WebPage;

public class BookmarkablePageMenuItem extends
		MenuItem<Class<? extends WebPage>> implements Serializable {
	private static final long serialVersionUID = 1L;
	public Class<? extends WebPage> page;

	public BookmarkablePageMenuItem(final Class<? extends WebPage> page) {
		this(getNameFromPage(page), page, false);
	}

	private static String getNameFromPage(final Class<? extends WebPage> page2) {
		final String simpleName = page2.getSimpleName();
		return simpleName.substring(0, simpleName.lastIndexOf("Page"));
	}

	public BookmarkablePageMenuItem(final String name,
			final Class<? extends WebPage> page) {
		this(name, page, false);
	}

	public BookmarkablePageMenuItem(final String name,
			final Class<? extends WebPage> page, final boolean isSelected) {
		super(name, isSelected);
		this.page = page;
	}

	public BookmarkablePageMenuItem(final Class<? extends WebPage> page,
			final List<MenuItem<?>> submenu) {
		this(getNameFromPage(page), page, false, submenu);
	}

	public BookmarkablePageMenuItem(final String name,
			final Class<? extends WebPage> page, final boolean isSelected,
			final List<MenuItem<?>> submenu) {
		super(name, isSelected, submenu);
		this.page = page;
	}

	@Override
	public Class<? extends WebPage> getLink() {
		return page;
	}

	public void setUrl(final Class<WebPage> page) {
		this.page = page;
	}
}
