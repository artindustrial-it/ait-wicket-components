package com.ait.wicket.component.menu;

import java.io.Serializable;
import java.util.List;

public abstract class MenuItem<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	public boolean showSubmenu;
//	public boolean selected;
	public List<MenuItem<?>> submenu;
	public String propertyKey;

	public MenuItem(final String propertyKey, final boolean isSelected) {
		this(propertyKey, isSelected, null);
	}

	public MenuItem(final String propertyKey, final boolean isSelected,
			final List<MenuItem<?>> submenu) {
		super();
		this.propertyKey = propertyKey;
//		this.selected = isSelected;
		this.submenu = submenu;
	}

	public boolean isSelected(final T pageClass) {
		return pageClass.equals(this.getLink());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean isThisOrAnyInSubmenuSelected(final T pageClass) {
		// if the actual item is selected
		if (isSelected(pageClass)) {
			return true;
		}
		// if any of the submenus is selected
		if (hasSubmenu()) {
			for (final MenuItem item : getSubmenu()) {
				if (item.isThisOrAnyInSubmenuSelected(pageClass)) {
					return true;
				}
			}
		}
		// if nothing in the tree is selected return false
		return false;
	}

	public List<MenuItem<?>> getSubmenu() {
		return submenu;
	}

	public void setSubmenu(final List<MenuItem<?>> submenu) {
		this.submenu = submenu;
	}

	public abstract T getLink();

	public boolean isDisabled() {
		// TODO MM implement logic
		return false;
	}

	public String getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(final String propertyKey) {
		this.propertyKey = propertyKey;
	}

	public boolean hasSubmenu() {
		return (submenu != null && submenu.size() > 0);
	}
}
