package com.ait.wicket.component.menu;

public interface SubmenuVisibilityProvider {
	boolean isShowSubmenu(ExternalUrlMenuItemPanel menuItemPanel);
}
