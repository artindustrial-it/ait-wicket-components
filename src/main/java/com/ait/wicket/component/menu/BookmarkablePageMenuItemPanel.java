package com.ait.wicket.component.menu;

import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;

public class BookmarkablePageMenuItemPanel extends AbstractMenuItemPanel<BookmarkablePageMenuItem> {
	private static final long serialVersionUID = 1L;

	public BookmarkablePageMenuItemPanel(final String id, final IModel<BookmarkablePageMenuItem> model) {
		super(id, model);
	}

	public BookmarkablePageMenuItemPanel(final String id, final IModel<BookmarkablePageMenuItem> model,
			final Behavior[] subMenuItemBehavior) {
		super(id, model, subMenuItemBehavior);
	}
	
	@SuppressWarnings("unchecked")
	public boolean isSelected() {
		final BookmarkablePageMenuItem menuItem = getModelObject();
		if (getPage() instanceof WebPage) {
			final Class<? extends WebPage> class1 = (Class<? extends WebPage>) getPage()
					.getClass();
			return menuItem
					.isThisOrAnyInSubmenuSelected(class1);
		}
		return false;
	}

	@Override
	protected AbstractLink createLink(String string) {
		return new BookmarkablePageLink<WebPage>(string,
				getModelObject().getLink());
	}
}
