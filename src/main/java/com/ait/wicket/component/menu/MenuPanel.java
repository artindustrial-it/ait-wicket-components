package com.ait.wicket.component.menu;

import java.util.List;

import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.ait.wicket.component.generics.TypedPanel;

/**
 * @author Manuel
 */
public class MenuPanel extends TypedPanel<List<MenuItem<?>>> {
	private static final long serialVersionUID = 1L;
	private ListView<MenuItem<?>> listView;
	private Behavior[] menuItemBehavior;
	private Behavior[] subMenuItemBehavior;

	public MenuPanel(final String id, final IModel<List<MenuItem<?>>> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		// TODO MM we do not want anonymous inner classes -> refactor
		listView = new ListView<MenuItem<?>>("menu", getModel()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<MenuItem<?>> item) {
				item.add(createMenuItemPanel(item.getModelObject()));
			}
		};
		add(listView);
	}

	protected AbstractMenuItemPanel<? extends MenuItem<?>> createMenuItemPanel(
			MenuItem<?> item) {
		AbstractMenuItemPanel<? extends MenuItem<?>> panel = null;
		if (item instanceof BookmarkablePageMenuItem) {
			panel = new BookmarkablePageMenuItemPanel(
					"item",
					Model.<BookmarkablePageMenuItem> of((BookmarkablePageMenuItem) item),
					subMenuItemBehavior);
		} else if (item instanceof ExternalUrlMenuItem) {
			panel = new ExternalUrlMenuItemPanel(
					"item",
					Model.<ExternalUrlMenuItem> of((ExternalUrlMenuItem) item),
					subMenuItemBehavior);
		} else if (item instanceof OpenCloseMenuItem) {
			panel = new OpenCloseMenuItemPanel(
					"item",
					Model.<OpenCloseMenuItem> of((OpenCloseMenuItem) item),
					subMenuItemBehavior);
		}
		if (menuItemBehavior != null) {
			panel.add(menuItemBehavior);
		}
		return panel;
	}

	public MenuPanel addMenuItemBehaviors(final Behavior... behavior) {
		menuItemBehavior = behavior;
		return this;
	}

	public MenuPanel addSubMenuItemBehaviors(final Behavior... behavior) {
		subMenuItemBehavior = behavior;
		return this;
	}
}
