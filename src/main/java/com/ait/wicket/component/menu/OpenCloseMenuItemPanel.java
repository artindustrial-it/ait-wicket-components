package com.ait.wicket.component.menu;

import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class OpenCloseMenuItemPanel extends AbstractMenuItemPanel<OpenCloseMenuItem> {

	private static final long serialVersionUID = 1L;
	private boolean open;

	public OpenCloseMenuItemPanel(final String id, final IModel<OpenCloseMenuItem> model) {
		super(id, model);
	}

	public OpenCloseMenuItemPanel(String id, Model<OpenCloseMenuItem> model,
			Behavior[] subMenuItemBehavior) {
		super(id, model, subMenuItemBehavior);
	}

	public AbstractLink createLink(String id) {
		return new Link<Void>(id) {
			private static final long serialVersionUID = 1L;

			/**
			 * This component get created newly when it is clicked. So the open state is never "stored" and it will always be closed...
			 * FIXME How will the open / close state correctly "stored"?
			 */
			@Override
			public void onClick() {
				System.out.println("Clicked open: " + open + " -> " + !open);
//				OpenCloseMenuItemPanel.this.getModelObject()
				OpenCloseMenuItemPanel.this.open = !open;
				System.out.println("new open: " + open + " - comp id:" + OpenCloseMenuItemPanel.this.hashCode());
			}
		};
	}
	
	protected CssClassEnum getCssClass() {
		final MenuItem<?> menuItem = getModelObject();
		if (menuItem.hasSubmenu() && open) {
				return CssClassEnum.OPEN;
		} else {
			return super.getCssClass();
		}
	}

	@Override
	protected boolean isSubmenuVisible(MenuItem<?> menuItem) {
		System.out.println("submenu visible: hasSub: " + menuItem.hasSubmenu() + " - open: " + open + " - comp id: " + hashCode());
		return menuItem.hasSubmenu() && open;
	}
}
