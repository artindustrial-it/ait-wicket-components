package com.ait.wicket.component.grouping.tablerow;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

/**
 * @author Yoav Aharoni
 */
public class GroupRow extends Panel {

	public GroupRow(String id, Object value, final Integer groupSize) {
		super(id);
		add(new Label("label", Model.of(value.toString())) {
			@Override
			public void onComponentTagBody(MarkupStream markupStream,
					ComponentTag openTag) {
				if (groupSize != null) {
					String body = getDefaultModelObjectAsString();
					body += "&nbsp;<span style=\"color:grey\">(" + groupSize + ")</span>";
					replaceComponentTagBody(markupStream, openTag, body);
				} else {
					super.onComponentTagBody(markupStream, openTag);
				}
			}
		});
	}
}
