package com.ait.wicket.component.grouping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.wicket.model.LoadableDetachableModel;

import com.ait.clazz.ClassUtils;
import com.ait.collection.BeanComparator;
import com.ait.collection.BeanComparator.Property;

public abstract class GroupingModel<E extends Serializable, F extends Serializable>
		extends LoadableDetachableModel<List<F>> implements IGroupingModel<E, F> {

	private List<List<F>> groups;
	private boolean isAlreadySorted;

	public GroupingModel() {
		this(false);
	}

	public GroupingModel(boolean isAlreadySorted) {
		this.isAlreadySorted = isAlreadySorted;
	}

	public List<F> getBodyObject(int i) {
		return getGroups().get(i);
	}

	public List<List<F>> getGroups() {
		// if the groups are null we have to load, sort and group the data
		if (groups == null) {
			groups = new ArrayList<List<F>>();
			List<F> list = super.getObject();
			ClassUtils.debug("size of original list: " + list.size());
			if (list.size() > 0) {
				if (!isAlreadySorted) {
					Property[] grouping = getGroupingProperties();
					Property[] sorting = getSortProperties();
					if (sorting != null && sorting.length > 0) {
						Collection<Property> groupAndSortList = new ArrayList<Property>(
								Arrays.asList(grouping));
						groupAndSortList.addAll(Arrays.asList(sorting));
						grouping = groupAndSortList.toArray(new Property[groupAndSortList
								.size()]);
					}
					Collections.sort(list, new BeanComparator(grouping));
					ClassUtils.debug("sorted: " + list.size());
				}
				List<F> group = null;
				F lastItem = null;
				for (F item : list) {
					// if we have to initialize first or group has changed
					if (lastItem == null || !hasSameGroup(lastItem, item)) {
						group = new ArrayList<F>();
						groups.add(group);
					}
					group.add(item);
					lastItem = item;
				}
				ClassUtils.debug("grouped...");
			}
		}
		return groups;
	}

	private boolean hasSameGroup(F lastItem, F item) {
		return new BeanComparator(getGroupingProperties()).compare(lastItem, item) == 0;
	}

	public void setBodyObject(int i, F object) {
		List<F> list = super.getObject();
		if (list != null && list.size() > i) {
			list.set(i, object);
		}
	}

	public Property[] getSortProperties() {
		return null;
	}

	@Override
	public void detach() {
		groups = null;
		super.detach();
	}
}
