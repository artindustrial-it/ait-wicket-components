package com.ait.wicket.component.grouping;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.ait.wicket.behavior.CssClassAppendingBehavior;
import com.ait.wicket.behavior.grid.CssClassLayout;
import com.ait.wicket.component.grouping.tablerow.GroupRow;

/**
 * @author Yoav Aharoni
 */
public class GroupingTable<T extends Serializable, S extends Serializable>
		extends DefaultDataTable<T, S> {

	private IChoiceRenderer<T> groupHeaderRenderer;

	public GroupingTable(String id, List<IColumn<T, S>> columns,
			ISortableDataProvider<T, S> dataProvider, int rowsPerPage) {
		super(id, columns, dataProvider, rowsPerPage);
	}

	public GroupingTable(String id, List<IColumn<T, S>> columns,
			ISortableDataProvider<T, S> dataProvider, int rowsPerPage, S groupBy) {
		this(id, columns, dataProvider, rowsPerPage);
		setItemReuseStrategy(new GrouingItemsStrategy<S>(this, groupBy));
	}

	public GroupingTable(String id, List<IColumn<T, S>> columns,
			ISortableDataProvider<T, S> dataProvider, int rowsPerPage, S groupBy,
			IChoiceRenderer<T> groupHeaderRenderer) {
		this(id, columns, dataProvider, rowsPerPage, groupBy);
		this.groupHeaderRenderer = groupHeaderRenderer;
	}

	/**
	 * Are Groups Expanded/Collapsed by default
	 * 
	 * @param groupRowItem
	 *          Group row item
	 * @return true if expanded by default, else false
	 */
	public boolean isGroupExpanded(Item<T> groupRowItem) {
		return true;
	}

	protected Item newGroupRowItem(String id, int index, IModel<T> model) {
		Item<T> item = new Item<T>(id, index, model);
		item.add(new CssClassAppendingBehavior(Model.of(new CssClassLayout(
				"group-header-row"))));
		if (isGroupExpanded(item)) {
			item.add(new CssClassAppendingBehavior(Model.of(new CssClassLayout(
					"group-expanded"))));
		} else {
			item.add(new CssClassAppendingBehavior(Model.of(new CssClassLayout(
					"group-collapsed"))));
		}
		return item;
	}

	protected Item<T> newGroupCellItem(String id, int index, IModel<T> model) {
		Item<T> item = new Item<T>(id, index, model);
		String colspan = String.valueOf(getColumns().size());
		item.add(new AttributeModifier("colspan", colspan));
		item.add(new CssClassAppendingBehavior(Model.of(new CssClassLayout(
				"first-cell last-cell"))));
		item.add(new AttributeAppender("onclick",
				"GroupableTable.collapseExpand(this); "));
		return item;
	}

	protected void populateGroupItem(Item cellItem, String componentId,
			S property, IModel<T> rowModel) {
		Object value = null;
		if (groupHeaderRenderer != null) {
			value = groupHeaderRenderer.getDisplayValue(rowModel.getObject());
		} else if (property != null) {
			value = PropertyResolver.getValue(property.toString(),
					rowModel.getObject());
		}
		cellItem.add(new GroupRow(componentId, value, null));
	}
}
