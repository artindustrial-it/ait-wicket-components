package com.ait.wicket.component.grouping;

import java.io.Serializable;
import java.util.List;

import com.ait.collection.BeanComparator.Property;

public interface IGroupingModel<E extends Serializable, F extends Serializable>
		extends Serializable {

	public E getHeaderObject(int i);

	public List<F> getBodyObject(int i);

	public Property[] getGroupingProperties();

	public Property[] getSortProperties();
}
