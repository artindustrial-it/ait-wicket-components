package com.ait.wicket.component.grouping;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.repeater.IItemFactory;
import org.apache.wicket.markup.repeater.IItemReuseStrategy;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.ait.wicket.behavior.CssClassAppendingBehavior;
import com.ait.wicket.behavior.grid.CssClassLayout;

/**
 * @author Yoav Aharoni
 */
public class GrouingItemsStrategy<S extends Serializable> implements
		IItemReuseStrategy {
	private GroupingTable table;
	private S groupBy;

	public GrouingItemsStrategy(GroupingTable table) {
		this.table = table;
	}

	public GrouingItemsStrategy(GroupingTable<?, S> table, S groupBy) {
		this(table);
		this.groupBy = groupBy;
	}

	public <T> Iterator<Item<T>> getItems(final IItemFactory<T> factory,
			final Iterator<IModel<T>> newModels, Iterator<Item<T>> existingItems) {
		return new Iterator<Item<T>>() {
			private int index = 0;

			private Object lastGroupValue;
			private Item<T> lastGroupItem;
			private IModel<T> lastGroupModel;

			public void remove() {
				throw new UnsupportedOperationException();
			}

			public boolean hasNext() {
				return lastGroupModel != null || newModels.hasNext();
			}

			@SuppressWarnings({ "unchecked" })
			public Item<T> next() {
				// returned group item in last iteration, return saved model item
				if (lastGroupModel != null) {
					Item<T> item = newRowItem(lastGroupModel);
					lastGroupModel = null;
					return item;
				}

				IModel<T> model = newModels.next();

				// check if grouped
				if (groupBy != null && model != null) {
					String property = groupBy.toString();
					// IChoiceRenderer renderer = provider.getRenderer(property);
					T modelObject = model.getObject();
					List<IColumn> cols = table.getColumns();
					if (cols != null && cols.size() > 0) {
						Object value = PropertyResolver.getValue(property, modelObject);
						// Object value = renderer.getIdValue(modelObject, index);
						if (isDifferent(value, lastGroupValue)) {
							lastGroupValue = value;
							lastGroupModel = model;

							lastGroupItem = table.newGroupRowItem("group" + index, index,
									model);
							Item cellItem = table.newGroupCellItem("cells", 0, model);
							lastGroupItem.add(cellItem);
							table.populateGroupItem(cellItem, "cell", property, model);
							return lastGroupItem;
						}
					}
				}

				return newRowItem(model);
			}

			private boolean isDifferent(Object value, Object lastGroupValue) {
				boolean result = false;
				if (value == null && lastGroupValue == null) {
					result = false;
				} else if (value == null || lastGroupValue == null) {
					result = true;
				} else {
					result = !value.equals(lastGroupValue);
				}
				return result;
			}

			@SuppressWarnings({ "unchecked" })
			private Item<T> newRowItem(IModel<T> model) {
				Item<T> item = factory.newItem(index, model);
				if (lastGroupItem != null && !table.isGroupExpanded(lastGroupItem)
						&& groupBy != null) {
					item.add(new CssClassAppendingBehavior(Model.of(new CssClassLayout(
							"row-collapsed"))));
				}
				index++;
				return item;
			}
		};
	}
}
