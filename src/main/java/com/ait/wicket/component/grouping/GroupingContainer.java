package com.ait.wicket.component.grouping;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;

/**
 * This {@link RepeatingView} can group a list of objects and put them into a
 * header and body {@link Component}.
 * 
 * For example if you have a table which you have to group to many tables by one
 * field separated through headers you can use this component.
 * 
 * @author Manuel
 * 
 * @param <G>
 *          headerRowModel
 * @param <H>
 *          bodyRowModel
 */
public abstract class GroupingContainer<G extends Serializable, H extends Serializable>
		extends Panel {

	private RepeatingView repeater;

	// private RefreshingView repeater1;

	public GroupingContainer(String id, GroupingModel<G, H> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		repeater = new RepeatingView("repeater");
		add(repeater);
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		if (repeater.isVisible()) {
			repeater.removeAll();
			for (int i = 0; i < getGroupsLength(); i++) {
				Fragment group = newGroupContainer(repeater.newChildId());
				repeater.add(group);
				group.add(newHeaderContainer("header", getModel().getHeaderObject(i)));
				group.add(newBodyContainer("body", getModel().getBodyObject(i)));
			}
		}
	}

	protected Fragment newGroupContainer(String id) {
		return new Fragment(id, "fragment.group", this);
	}

	public GroupingModel<G, H> getModel() {
		return (GroupingModel<G, H>) getDefaultModel();
	}

	public void setModel(GroupingModel<G, H> model) {
		setDefaultModel(model);
	}

	public int getGroupsLength() {
		return getModel().getGroups().size();
	}

	protected abstract Component newHeaderContainer(String id, G headerRowModel);

	protected abstract Component newBodyContainer(String id, List<H> bodyRowModel);

}
