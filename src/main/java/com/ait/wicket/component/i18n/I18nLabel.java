package com.ait.wicket.component.i18n;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.ait.wicket.model.MultilineConverterModel;

/**
 * This component generates a label which gets its content from a property file,
 * so multi-language environments can easily created. Just create a property for
 * your language.
 * 
 * Another feature is, it behaves like an {@link MultiLineLabel} when needed
 * (the display text contains '\n' or '\r')
 * 
 * @author Manuel
 * 
 */
public class I18nLabel extends Label {
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the Model-Object from a propertyfile, with the key named like the id
	 * 
	 * @param id
	 */
	public I18nLabel(final String id) {
		this(id, id);
	}

	public I18nLabel(final String id, final String propertyKey) {
		this(id, Model.of(propertyKey));
	}

	public I18nLabel(final String id, final IModel<String> propertyKeyModel) {
		super(id);
		setDefaultModel(new MultilineConverterModel(new I18nModel(
				propertyKeyModel.getObject(), this)));
		// TODO MM XSS attack possible? (we do not escape the model content,
		// needed because of the multiline model)
		setEscapeModelStrings(false);
	}

	public Component addFor(final Component forComponent) {
		forComponent.setOutputMarkupId(true);
		add(new AttributeAppender("for", Model.of(forComponent.getMarkupId()), " "));
		return this;
	}
}
