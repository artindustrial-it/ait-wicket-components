package com.ait.wicket.component.i18n;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

public class I18nModel extends AbstractReadOnlyModel<String> {
	private static final long serialVersionUID = 1L;
	private final String propertyKey;
	private final Component component;

	/**
	 * Actually this is not deprecated but strange to use, as it won't work when
	 * defining resources on panels, borders, etc. (only on page level)
	 * 
	 * @param propertyKey
	 */
	@Deprecated
	public I18nModel(final String propertyKey) {
		this(propertyKey, null);
	}

	public I18nModel(final String propertyKey, final Component component) {
		super();
		this.propertyKey = propertyKey;
		this.component = component;
	}

	@Override
	public String getObject() {
		return Application.get().getResourceSettings().getLocalizer()
				.getString(propertyKey, component, null, propertyKey);
	}

	/**
	 * Do only use that when you know <b>exactly</b> what you are doing.
	 * 
	 * @see {@link I18nModel#I18nModel(String)}
	 * @deprecated
	 * @param key
	 * @return
	 */
	@Deprecated
	public static IModel<String> of(final String key) {
		return new I18nModel(key);
	}

	public static IModel<String> of(final String key, final Component component) {
		return new I18nModel(key, component);
	}

	// TODO this will not work in the most cases, fix constructor
	public static String getResource(final String string) {
		return new I18nModel(string).getObject();
	}

}
