package com.ait.wicket.component.javascript;

import java.io.Serializable;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.ait.wicket.component.generics.TypedPanel;
import com.ait.wicket.component.javascript.AutoRefreshPagePanel.RefreshOptions;

public class AutoRefreshPagePanel extends TypedPanel<RefreshOptions> {

	private Label scriptLabel;

	public AutoRefreshPagePanel(String id, IModel<RefreshOptions> model) {
		super(id, model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		scriptLabel = new Label("script", new Model<String>());
		add(scriptLabel);
		scriptLabel.setEscapeModelStrings(false);
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		RefreshOptions options = getModelObject();
		if (options == null || !options.isRefresh()) {
			scriptLabel.setVisible(false);
		} else {
			scriptLabel.setVisible(true);
			String refreshString = (options.getUrl() == null
					|| options.getUrl().length() == 0 ? "location.refresh(true);"
					: "	location = '" + options.getUrl() + "';");
			long refreshTime = (options.getRefreshTime() == null
					|| options.getRefreshTime() <= 0 ? 5000l
					: options.getRefreshTime() * 1000);
			scriptLabel.setDefaultModelObject("setTimeout(function () {"
					+ refreshString + "}, " + refreshTime + ");");
		}
	}

	public static class RefreshOptions implements Serializable {
		private boolean refresh;
		private String url;
		private Long refreshTime;

		public boolean isRefresh() {
			return refresh;
		}

		public RefreshOptions setRefresh(boolean refresh) {
			this.refresh = refresh;
			return this;
		}

		public String getUrl() {
			return url;
		}

		public RefreshOptions setUrl(String url) {
			this.url = url;
			return this;
		}

		public Long getRefreshTime() {
			return refreshTime;
		}

		public RefreshOptions setRefreshTime(Long refreshTime) {
			this.refreshTime = refreshTime;
			return this;
		}
	}

}
