package com.ait.wicket.component.chart.option;

public class MarkerSettings extends AbstractSettings {
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>(
			"show");
	private static final SettingKey<String> POSITION = new SettingKey<String>(
			"position");

	public MarkerSettings() {
		this(true);
	}

	public MarkerSettings(Boolean show) {
		if (show != null)
			setShow(show);
	}

	@Override
	public String getName() {
		return "markers";
	}
	
	public String getPosition() {
		return get(POSITION);
	}

	/**
	 * @param position
	 *          e.g. nw, n, se,...
	 */
	public MarkerSettings setPosition(String position) {
		set(POSITION, position);
		return this;
	}

	public Boolean getShow() {
		return get(SHOW);
	}

	public MarkerSettings setShow(Boolean show) {
		set(SHOW, show);
		return this;
	}
}