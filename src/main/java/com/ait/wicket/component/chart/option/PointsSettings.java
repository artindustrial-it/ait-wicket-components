package com.ait.wicket.component.chart.option;

public class PointsSettings extends AbstractSettings {
	public PointsSettings() {
		this(true);
	}
	
	public PointsSettings(Boolean show) {
		super();
		if (show != null) {
			setShow(show);
		}
	}
	
	@Override
	public String getName() {
		return "points";
	}
	
	public static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>("show");

	public PointsSettings setShow(Boolean show) {
		set(SHOW, show);
		return this;
	}

	public Boolean getShow() {
		return get(SHOW);
	}
}
