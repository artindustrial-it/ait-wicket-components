package com.ait.wicket.component.chart;

import java.io.Serializable;

public interface Coordinates<T, V> extends Serializable {

	public void setX(T x);

	public T getX();

	public void setY(V y);

	public V getY();
}