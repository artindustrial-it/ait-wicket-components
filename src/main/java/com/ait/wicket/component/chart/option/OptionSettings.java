package com.ait.wicket.component.chart.option;

import java.io.Serializable;

public interface OptionSettings extends Serializable {
	public String toFlotrString();
	
	public boolean mergeSettings(OptionSettings item);

}