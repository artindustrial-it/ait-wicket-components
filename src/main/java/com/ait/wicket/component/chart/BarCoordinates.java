package com.ait.wicket.component.chart;

import java.util.Date;

public class BarCoordinates implements Coordinates<Date, Float> {
	private static final long serialVersionUID = 1L;

	private Date x;
	private float y;

	public BarCoordinates(Date x, float y) {
		this.setX(x);
		this.setY(y);
	}

	public Date getX() {
		return x;
	}

	public void setX(Date x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}
}