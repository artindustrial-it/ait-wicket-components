package com.ait.wicket.component.chart.option;

/**
 * @author Manuel
 */
public class LineChartSettings extends AbstractSettings {

	private static final SettingKey<Integer> LINE_WIDTH = new SettingKey<Integer>(
			"lineWidth");
	private static final SettingKey<Boolean> FILL = new SettingKey<Boolean>(
			"fill");
	private static final SettingKey<String> FILL_COLOR = new SettingKey<String>(
			"fillColor");
	private static final SettingKey<Boolean> FILL_BORDER = new SettingKey<Boolean>(
			"fillBorder");
	private static final SettingKey<Boolean> STACKED = new SettingKey<Boolean>(
			"stacked");
	private static final SettingKey<Boolean> STEPS = new SettingKey<Boolean>(
			"steps");
	private static final SettingKey<Float> FILL_OPACITY = new SettingKey<Float>(
			"fillOpacity");
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>(
			"show");

	public LineChartSettings() {
		this(true);
	}

	public LineChartSettings(Boolean isLineChart) {
		super();
		if (isLineChart != null) {
			set(SHOW, isLineChart);
		}
	}

	@Override
	public String getName() {
		return "lines";
	}

	public Integer getLineWidth() {
		return get(LINE_WIDTH);
	}

	/**
	 * @param lineWidth
	 *          the line width in pixels
	 */
	public LineChartSettings setLineWidth(Integer lineWidth) {
		set(LINE_WIDTH, lineWidth);
		return this;
	}

	public Boolean getFill() {
		return get(FILL);
	}

	/**
	 * @param fill
	 *          true to fill the area from the line to the x axis, false for
	 *          (transparent) no fill
	 */
	public LineChartSettings setFill(Boolean fill) {
		set(FILL, fill);
		return this;
	}

	public String getFillColor() {
		return get(FILL_COLOR);
	}

	/**
	 * @param fillColor
	 *          fill color
	 */
	public LineChartSettings setFillColor(String fillColor) {
		set(FILL_COLOR, fillColor);
		setFill(true);
		return this;
	}

	public Float getFillOpacity() {
		return get(FILL_OPACITY);
	}

	/**
	 * @param fillOpacity
	 *          opacity of the fill color, set to 1 for a solid fill, 0 hides the
	 *          fill
	 */
	public LineChartSettings setFillOpacity(Float fillOpacity) {
		set(FILL_OPACITY, fillOpacity);
		setFill(true);
		return this;
	}

	public Boolean getFillBorder() {
		return get(FILL_BORDER);
	}

	/**
	 * @param fillBorder
	 *          fill border
	 */
	public LineChartSettings setFillBorder(Boolean fillBorder) {
		set(FILL_BORDER, fillBorder);
		setFill(true);
		return this;
	}

	public Boolean getSteps() {
		return get(STEPS);
	}

	/**
	 * @param steps
	 *          draw steps
	 */
	public LineChartSettings setSteps(Boolean steps) {
		set(STEPS, steps);
		return this;
	}

	public Boolean getStacked() {
		return get(STACKED);
	}

	/**
	 * @param stacked
	 *          true will show stacked lines, false will show normal lines
	 */
	public LineChartSettings setStacked(Boolean stacked) {
		set(STACKED, stacked);
		return this;
	}
}
