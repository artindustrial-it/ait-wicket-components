package com.ait.wicket.component.chart;

import java.io.Serializable;

public class BarChartData implements Serializable {
	private static final long serialVersionUID = 1L;

	private BarSequence[] barSequence;

	public BarChartData(BarSequence[] barSequence) {
		this.barSequence = barSequence;
	}

	public BarSequence[] getBarSequence() {
		return barSequence;
	}

	public void setBarSequence(BarSequence[] barSequence) {
		this.barSequence = barSequence;
	}
}