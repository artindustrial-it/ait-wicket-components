package com.ait.wicket.component.chart.option;

/**
 * @author Manuel
 */
public class LegendSettings extends AbstractSettings {
	public static final String NO_COLOR = "none";

	private static final SettingKey<String> POSITION = new SettingKey<String>(
			"position");
	private static final SettingKey<String> BACKGROUND_COLOR = new SettingKey<String>(
			"backgroundColor");
	private static final SettingKey<Float> BACKGROUND_OPACITY = new SettingKey<Float>(
			"backgroundOpacity");
	private static final SettingKey<String> BOX_BORDER_COLOR = new SettingKey<String>(
			"labelBoxBorderColor");
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>(
			"show");
	private static final SettingKey<String> LABEL_CONTAINER = new SettingKey<String>(
			"container", true);

	public LegendSettings() {
		super();
	}

	@Override
	public String getName() {
		return "legend";
	}

	public String getPosition() {
		return get(POSITION);
	}

	/**
	 * @param position
	 *          e.g. nw, n, se,...
	 */
	public LegendSettings setPosition(String position) {
		set(POSITION, position);
		return this;
	}

	public String getBackgroundColor() {
		return get(BACKGROUND_COLOR);
	}

	/**
	 * @param backgroundColor
	 *          the background color of the legend (set
	 *          {@link LegendSettings#NO_COLOR} if you do not want any background
	 *          color at all)
	 */
	public LegendSettings setBackgroundColor(String backgroundColor) {
		set(BACKGROUND_COLOR, backgroundColor);
		return this;
	}

	public Float getBackgroundOpacity() {
		return get(BACKGROUND_OPACITY);
	}

	/**
	 * @param backgroundOpacity
	 *          the opacity of the background
	 */
	public LegendSettings setBackgroundOpacity(Float backgroundOpacity) {
		set(BACKGROUND_OPACITY, backgroundOpacity);
		return this;
	}

	public String getBoxBorderColor() {
		return get(BOX_BORDER_COLOR);
	}

	/**
	 * @param boxBorderColor
	 *          the color of the legend's border box (set
	 *          {@link LegendSettings#NO_COLOR} if you do not want any border at
	 *          all)
	 */
	public LegendSettings setBoxBorderColor(String boxBorderColor) {
		set(BOX_BORDER_COLOR, boxBorderColor);
		return this;
	}

	public Boolean getShow() {
		return get(SHOW);
	}

	/**
	 * @param backgroundColor
	 *          set true if the legend should be shown, false if not
	 */
	public LegendSettings setShow(Boolean show) {
		set(SHOW, show);
		return this;
	}

	public String getLabelContainer() {
		return get(LABEL_CONTAINER);
	}

	/**
	 * This function sets a document.getElementById(...) around the given
	 * {@link String}.
	 * 
	 * @param labelContainer
	 *          if the legend should be printed in a separate container, give the
	 *          id here
	 */
	public LegendSettings setLabelContainer(String labelContainer) {
		setLabelContainerFunction("document.getElementById('" + labelContainer
				+ "')");
		return this;
	}

	/**
	 * @param labelContainerFunction
	 *          you can find your container node by the js function you give here
	 * @see LegendSettings#setLabelContainer(String)
	 */
	public LegendSettings setLabelContainerFunction(String labelContainerFunction) {
		set(LABEL_CONTAINER, labelContainerFunction);
		return this;
	}
}
