package com.ait.wicket.component.chart;

import java.io.Serializable;

public interface Line<T extends Coordinates<?, ?>> extends Serializable {

	public String getLabel();

	public void setLabel(String label);

	public T[] getPoints();

	public void setPoints(T[] points);

	public boolean isAlternateYaxis();

	public String getColor();

	public void setColor(String color);
}