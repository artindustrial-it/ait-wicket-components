package com.ait.wicket.component.chart.option;

public class RootSettings extends AbstractSettings {
	public RootSettings() {
		super();
		set(new ColorSettings());
		set(new GridSettings());
		set(new XaxisSettings());
		set(new X2AxisSettings());
		set(new YaxisSettings());
		set(new Y2AxisSettings());
		set(new PieChartSettings(null));
		set(new LineChartSettings(null));
		set(new BarChartSettings(null));
		set(new PointsSettings(null));
		set(new MouseSettings());
		set(new LegendSettings());
		set(new MarkerSettings(null));
		set(new HtmlTextSetting());
	}

	public static final SettingKey<Boolean> POINTS = new SettingKey<Boolean>("points");
	
}
