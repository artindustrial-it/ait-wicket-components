package com.ait.wicket.component.chart;


public class Timeline implements Line<TimeCoordinates> {
	private static final long serialVersionUID = 1L;
	
	private String label;
	private TimeCoordinates[] points;
	private boolean alternateYaxis;
	private String color;

	public Timeline(String label, TimeCoordinates[] points) {
		setLabel(label);
		setPoints(points);
	}

	public Timeline(String label, TimeCoordinates[] points, boolean alternateYaxis) {
		this(label, points);
		setAlternateYaxis(alternateYaxis);
	}
	
	public Timeline(String label, TimeCoordinates[] points, String color, boolean alternateYaxis) {
		this(label, points);
		this.color = color;
		this.alternateYaxis = alternateYaxis;
	}
	
	public TimeCoordinates[] getPoints() {
		return points;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setPoints(TimeCoordinates[] points) {
		this.points = points;
	}

	public boolean isAlternateYaxis() {
		return alternateYaxis;
	}

	public void setAlternateYaxis(boolean alternateYaxis) {
		this.alternateYaxis = alternateYaxis;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}