package com.ait.wicket.component.chart;

import java.io.Serializable;

public class Slice implements Serializable {
	private static final long serialVersionUID = 1L;
	String label;
	float value;

	public Slice(String label, float value) {
		this.label = label;
		this.value = value;
	}
}
