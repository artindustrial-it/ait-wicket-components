package com.ait.wicket.component.chart.option;

/**
 * @author Manuel
 */
public class BarChartSettings extends AbstractSettings {

	private static final SettingKey<Boolean> STACKED = new SettingKey<Boolean>(
			"stacked");
	private static final SettingKey<Boolean> HORIZONTAL = new SettingKey<Boolean>(
			"horizontal");
	private static final SettingKey<Integer> LINE_WIDTH = new SettingKey<Integer>(
			"lineWidth");
	private static final SettingKey<Float> BAR_WIDTH = new SettingKey<Float>(
			"barWidth");
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>(
			"show");

	public BarChartSettings() {
		this(true);
	}

	public BarChartSettings(Boolean isBarChart) {
		super();
		if (isBarChart != null) {
			set(SHOW, isBarChart);
		}
	}

	@Override
	public String getName() {
		return "bars";
	}

	public Integer getLineWidth() {
		return get(LINE_WIDTH);
	}

	/**
	 * @param lineWidth
	 *          the line width in pixels
	 */
	public BarChartSettings setLineWidth(Integer lineWidth) {
		set(LINE_WIDTH, lineWidth);
		return this;
	}

	public Boolean getHorizontal() {
		return get(HORIZONTAL);
	}

	/**
	 * @param horizontal
	 *          true to display horizontal bars, false for vertical bars
	 */
	public BarChartSettings setHorizontal(Boolean horizontal) {
		set(HORIZONTAL, horizontal);
		return this;
	}

	public Float getBarWidth() {
		return get(BAR_WIDTH);
	}

	/**
	 * @param fillOpacity
	 *          opacity of the fill color, set to 1 for a solid fill, 0 hides the
	 *          fill
	 */
	public BarChartSettings setBarWidth(Float barWidth) {
		set(BAR_WIDTH, barWidth);
		return this;
	}

	public Boolean getStacked() {
		return get(STACKED);
	}

	/**
	 * @param stacked
	 *          true will show stacked lines, false will show normal lines
	 */
	public BarChartSettings setStacked(Boolean stacked) {
		set(STACKED, stacked);
		return this;
	}
}
