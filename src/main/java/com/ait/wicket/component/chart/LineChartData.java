package com.ait.wicket.component.chart;

import java.io.Serializable;


public interface LineChartData<T extends Line<?>> extends Serializable {

	public T[] getLines();

	public void setLines(T[] lines);
}