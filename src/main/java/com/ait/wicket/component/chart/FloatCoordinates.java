package com.ait.wicket.component.chart;


public class FloatCoordinates implements Coordinates<Float, Float> {
	private static final long serialVersionUID = 1L;

	private float x;
	private float y;

	public FloatCoordinates(float x, float y) {
		this.setX(x);
		this.setY(y);
	}

	public Float getX() {
		return x;
	}

	public void setX(Float x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}

	public float getFlotrX() {
		return getX();
	}

	public float getFlotrY() {
		return getY();
	}
}