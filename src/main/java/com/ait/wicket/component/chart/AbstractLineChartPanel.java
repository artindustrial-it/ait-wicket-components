package com.ait.wicket.component.chart;

import org.apache.wicket.model.IModel;

import com.ait.wicket.component.chart.option.GridSettings;
import com.ait.wicket.component.chart.option.LineChartSettings;
import com.ait.wicket.component.chart.option.OptionSettings;
import com.ait.wicket.component.chart.option.PointsSettings;
import com.ait.wicket.component.chart.option.YaxisSettings;

/**
 * Das Line Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see BaseChartPanel
 * @author Manuel
 * @param <E>
 * @param <F>
 */
public abstract class AbstractLineChartPanel<T extends LineChartData<? extends Line<? extends Coordinates<E, F>>>, E, F>
		extends BaseChartPanel<T> {
	private static final long serialVersionUID = 1L;

	public AbstractLineChartPanel(String id, IModel<T> model) {
		super(id, model);
	}

	public AbstractLineChartPanel(String id, IModel<T> model,
			OptionSettings... settings) {
		super(id, model, settings);
	}

	@Override
	protected String convertDataToString(T filteredData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.getLines().length > 0) {
			// add all lines
			int lineCount = 0;
			for (Line<? extends Coordinates<E, F>> line : filteredData.getLines()) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				int coordCount = 0;
				if (line.getPoints() != null && line.getPoints().length > 0) {
					for (Coordinates<E, F> coord : line.getPoints()) {
						if (coordCount > 0) {
							sb.append(",");
						}
						sb.append("[" + convertX(coord.getX()) + ","
								+ convertY(coord.getY()) + "]");
						coordCount++;
					}
				}
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(line.getLabel());
				sb.append("'");
				if (line.isAlternateYaxis()) {
					sb.append(", yaxis: 2");
				}
				if (line.getColor() != null) {
					sb.append(", color: '" + line.getColor() + "'");
				}
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}

	protected abstract String convertX(E x);

	protected abstract String convertY(F y);

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] { new LineChartSettings(),
				new PointsSettings(), new YaxisSettings().setShowLabels(true),
				new GridSettings().setMinorVerticalLines(false), };
	}
}
