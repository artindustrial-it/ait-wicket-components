package com.ait.wicket.component.chart.option;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.ait.clazz.ClassUtils;

public abstract class AbstractSettings implements OptionSettings {
	private Map<Class<AbstractSettings>, AbstractSettings> subSettings;
	private Map<SettingKey<?>, Object> settingMap;

	public AbstractSettings() {
		setMap(new HashMap<SettingKey<?>, Object>());
		subSettings = new HashMap<Class<AbstractSettings>, AbstractSettings>();
	}

	protected String toFlotrString(int indentation) {
		if (!settingMap.isEmpty() || !subSettings.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			// open setting
			if (getName() != null) {
				appendIndented(sb, indentation++, getName(), ": {\n");
			}
			// print values or subsettings
			if (!settingMap.isEmpty()) {
				for (SettingKey<?> key : settingMap.keySet()) {
					Object value = settingMap.get(key);
					if (value != null) {
						appendSettingToFlotrString(indentation, sb, key, value);
					}
				}
			}
			if (!subSettings.isEmpty()) {
				for (AbstractSettings setting : subSettings.values()) {
					sb.append(setting.toFlotrString(indentation));
				}
			}
			if (getName() != null && sb.indexOf(",") > 0) {
				int start = sb.lastIndexOf(",");
				sb.replace(start, start + 2, "");
				appendIndented(sb, --indentation, "\n},\n");
			}
			return sb.toString();
		}
		return "";
	}

	protected void appendSettingToFlotrString(int indentation, StringBuilder sb,
			SettingKey<?> key, Object value) {
		appendIndented(sb, indentation, key.getKeyName(), ": ");
		sb.append(convertSettingValueToFlotrString(key, value));
		sb.append(",\n");
	}

	/**
	 * Override if you just want to format the value in a specific way (e.g.
	 * JavascriptFunctions would be a String but have to be without the leading &
	 * trailing '.
	 * 
	 * @param key
	 *          to identify which object we have here
	 * @param value
	 *          the value
	 * @return
	 */
	protected String convertSettingValueToFlotrString(SettingKey<?> key,
			Object value) {
		if (value instanceof String) {
			if (key.isFunction()) {
				return value != null ? value.toString() : "";
			} else {
				return "'" + value + "'";
			}
		} else if (value instanceof Number) {
			return value != null ? value.toString() : "";
		} else if (value instanceof Boolean) {
			return value != null ? value.toString() : "";
		} else if (value instanceof OptionSettings) {
			return ((OptionSettings) value).toFlotrString();
		} else if (value instanceof Collection<?>) {
			Collection<?> collection = (Collection<?>) value;
			if (!collection.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				sb.append("[");
				for (Object o : collection) {
					sb.append("\"").append(o).append("\",");
				}
				int start = sb.lastIndexOf(",");
				sb.replace(start, start + 1, "");
				sb.append("]");
				return sb.toString();
			}
			return "";
		} else {
			throw new IllegalStateException("Could not convert '" + key.getKeyName()
					+ "'. Please implement " + this.getClass().getSimpleName() + "."
					+ ClassUtils.getMethodName());
		}
	}

	protected final void appendIndented(StringBuilder sb, int indentation,
			String... strings) {
		if (strings != null && strings.length > 0) {
			for (int i = 0; i < indentation; i++) {
				sb.append("\t");
			}
			for (String string : strings) {
				sb.append(string);
			}
		}
	}

	/**
	 * Override me, if your setting has to print like the following example:
	 * 
	 * <pre>
	 *  settingName: {
	 *  	s1: v1,
	 *  	s2: v2,
	 *  	...
	 *  }
	 * </pre>
	 * 
	 * @return settingName
	 */
	public String getName() {
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(SettingKey<T> key) {
		return (T) settingMap.get(key);
	}

	public <T extends Object> AbstractSettings set(SettingKey<T> key, T value) {
		if (value instanceof AbstractSettings) {
			set((AbstractSettings) value);
		} else {
			settingMap.put(key, value);
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public AbstractSettings set(AbstractSettings settings) {
		Class<? extends AbstractSettings> settingsClass = settings.getClass();
		if (subSettings.containsKey(settingsClass)) {
			AbstractSettings savedSettings = subSettings.get(settingsClass);
			savedSettings.mergeSettings(settings);
		} else {
			subSettings.put((Class<AbstractSettings>) settingsClass, settings);
		}
		return this;
	}

	public <T extends Object> void remove(SettingKey<T> key) {
		settingMap.remove(key);
	}

	public boolean mergeSettings(OptionSettings setting) {
		boolean result = false;
		if (setting != null && setting instanceof AbstractSettings) {
			Class<? extends OptionSettings> itemClass = setting.getClass();
			if (itemClass.equals(this.getClass())) {
				Map<SettingKey<?>, Object> otherMap = ((AbstractSettings) setting)
						.getMap();
				for (SettingKey<?> key : otherMap.keySet()) {
					settingMap.put(key, otherMap.get(key));
				}
			} else if (subSettings.containsKey(itemClass)) {
				return subSettings.get(itemClass).mergeSettings(setting);
			} else {
				for (AbstractSettings subSetting : subSettings.values()) {
					result = subSetting.mergeSettings(setting);
					if (result) {
						return result;
					}
				}
			}
		}
		return result;
	}

	private Map<SettingKey<?>, Object> getMap() {
		return settingMap;
	}

	private void setMap(Map<SettingKey<?>, Object> map) {
		this.settingMap = map;
	}

	public static class SettingKey<T> implements Serializable {
		private final String keyName;
		private final boolean function;

		public SettingKey(String keyName) {
			this(keyName, false);
		}

		/**
		 * Use this, if you have a {@link String} value but you want to use it as a
		 * js-function.<br/>
		 * 
		 * Normally the
		 * {@link AbstractSettings#convertSettingValueToFlotrString(SettingKey, Object)}
		 * will put '' around the value, but if you instanciate your key with
		 * function=true, it will not do that.
		 * 
		 * The code
		 * <pre>
		 * {@code
		 * SettingKey colorKey = new SettingKey("color");
		 * set(colorKey, "#ccc"); 
		 * }
		 * </pre>
		 * results in something like (please note the ' at the beginning and end of the value): 
		 * <pre>
		 * {@code
		 * ...
		 * color: '#ccc', 
		 * ...
		 * }
		 * </pre>
		 * whereas following code
		 * <pre>
		 * {@code
		 * SettingKey containerIdKey = new SettingKey("containerId", true);
		 * set(containerIdKey, "document.getElementById('myId')"); 
		 * }
		 * </pre>
		 * results in (please note there is nothing at the beginning and end of the value):
		 * <pre>
		 * {@code
		 * ...
		 * containerId: document.getElementById('myId'),  
		 * ...
		 * }
		 * </pre>
		 * 
		 * @param keyName
		 * @param function
		 */
		public SettingKey(String keyName, boolean function) {
			this.keyName = keyName;
			this.function = function;
		}

		public boolean isFunction() {
			return function;
		}

		public String getKeyName() {
			return keyName;
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends OptionSettings> T findSetting(Class<T> setting) {
		if (subSettings.size() > 0) {
			if (subSettings.containsKey(setting)) {
				return (T) subSettings.get(setting);
			} else {
				// find recursive in tree
				for (AbstractSettings child : subSettings.values()) {
					T find = child.findSetting(setting);
					if (find != null) {
						return find;
					}
				}
			}
		}
		return null;
	}

	public final String toFlotrString() {
		return toFlotrString(0);
	}

	@Override
	public String toString() {
		return super.toString() + " - " + toFlotrString();
	}
}
