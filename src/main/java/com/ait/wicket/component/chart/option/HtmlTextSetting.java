package com.ait.wicket.component.chart.option;


/**
 * wether to draw the text using HTML or on the canvas
 * 
 * @author Manuel
 * 
 */
public class HtmlTextSetting extends AbstractSettings {

	private static final SettingKey<Boolean> HTML_TEXT = new SettingKey<Boolean>(
			"HtmlText");

	public HtmlTextSetting() {
	}

	public HtmlTextSetting setHtmlText(Boolean htmlText) {
		set(HTML_TEXT, htmlText);
		return this;
	}

	public Boolean getHtmlText() {
		return get(HTML_TEXT);
	}
}
