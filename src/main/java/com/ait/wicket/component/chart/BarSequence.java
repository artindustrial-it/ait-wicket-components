package com.ait.wicket.component.chart;

import java.io.Serializable;

public class BarSequence implements Serializable {
	private static final long serialVersionUID = 1L;

	private Bar[] bars;
	private String label;

	public BarSequence(Bar[] bars) {
		setBars(bars);
	}

	public BarSequence(String label, Bar[] bars) {
		this(bars);
		setLabel(label);
	}
	
	public Bar[] getBars() {
		return bars;
	}

	public void setBars(Bar[] bars) {
		this.bars = bars;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}