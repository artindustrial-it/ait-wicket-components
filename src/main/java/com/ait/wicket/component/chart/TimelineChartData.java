package com.ait.wicket.component.chart;


public class TimelineChartData implements LineChartData<Timeline> {
	private static final long serialVersionUID = 1L;

	private Timeline[] lines;

	public TimelineChartData(Timeline[] lines) {
		this.lines = lines;
	}

	public Timeline[] getLines() {
		return lines;
	}

	public void setLines(Timeline[] lines) {
		this.lines = lines;
	}
}