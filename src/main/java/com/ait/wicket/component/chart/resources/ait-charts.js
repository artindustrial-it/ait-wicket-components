var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

function getFormattedDate(timeStamp) {
	var string = float2int(timeStamp).toString();
	if (string.length == 2) {
		return "'" + string;
	} else if (string.length == 4) {
		return string;
	} else if (string.length == 6) {
		var year = string.substring(0, 4);
		var month = string.substring(4);
		return monthNames[month-1] + " " + year;
	} else if (string.length == 8) {
		var year = string.substring(0, 4);
		var month = string.substring(4, 6);
		var day = string.substring(6);
		return day + "." + month + "." + year;
	}
}

function getFormattedTime(timeStamp) {
	var string = float2int(timeStamp).toString();
	if (string.length == 2) {
		return "'" + string;
	} else if (string.length == 4) {
		return string;
	} else if (string.length == 6) {
		var year = string.substring(0, 4);
		var month = string.substring(4);
		return monthNames[month-1] + " " + year;
	} else if (string.length == 8) {
		var year = string.substring(0, 4);
		var month = string.substring(4, 6);
		var day = string.substring(6);
		return day + "." + month + "." + year;
	}
}

function float2int (value) {
    return value | 0;
}