package com.ait.wicket.component.chart;

public class FloatLine implements Line<FloatCoordinates> {
	private static final long serialVersionUID = 1L;

	private String label;
	private FloatCoordinates[] points;
	private boolean alternateYaxis;
	private String color;

	public FloatLine(String label, FloatCoordinates[] points) {
		setLabel(label);
		setPoints(points);
	}

	public FloatLine(String label, FloatCoordinates[] points,
			boolean alternateYAxis) {
		this(label, points);
		setAlternateYaxis(alternateYAxis);
	}

	public FloatCoordinates[] getPoints() {
		return points;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setPoints(FloatCoordinates[] points) {
		this.points = points;
	}

	public boolean isAlternateYaxis() {
		return alternateYaxis;
	}

	public void setAlternateYaxis(boolean alternateYaxis) {
		this.alternateYaxis = alternateYaxis;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}