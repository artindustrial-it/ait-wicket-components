package com.ait.wicket.component.chart;


public class FloatLineChartData implements LineChartData<FloatLine> {
	private FloatLine[] lines;

	public FloatLineChartData(FloatLine[] lines) {
		this.lines = lines;
	}

	public FloatLine[] getLines() {
		return lines;
	}

	public void setLines(FloatLine[] lines) {
		this.lines = lines;
	}
}