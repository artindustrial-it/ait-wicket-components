package com.ait.wicket.component.chart.option;

/**
 * wether to draw the text using HTML or on the canvas
 * 
 * @author Manuel
 * 
 */
public class PieChartSettings extends AbstractSettings {

	public PieChartSettings() {
		this(true);
	}

	public PieChartSettings(Boolean isPieChart) {
		super();
		if (isPieChart != null) {
			set(SHOW, isPieChart);
		}
	}

	@Override
	public String getName() {
		return "pie";
	}

	/** in pixels */
	private static final SettingKey<Integer> LINE_WIDTH = new SettingKey<Integer>(
			"lineWidth");
	/**
	 * true to fill the area from the line to the x axis, false for (transparent)
	 * no fill
	 */
	private static final SettingKey<Boolean> FILL = new SettingKey<Boolean>(
			"fill");
	/** fill color */
	private static final SettingKey<String> FILL_COLOR = new SettingKey<String>(
			"fillColor");
	/**
	 * opacity of the fill color, set to 1 for a solid fill, 0 hides the fill
	 */
	private static final SettingKey<Float> FILL_OPACITY = new SettingKey<Float>(
			"fillOpacity");
	/**
	 * the number of pixels the splices will be far from the center
	 */
	private static final SettingKey<Integer> EXPLODE = new SettingKey<Integer>(
			"explode");
	/**
	 * the size ratio of the pie relative to the plot
	 */
	private static final SettingKey<Float> SIZE_RATIO = new SettingKey<Float>(
			"sizeRatio");
	/**
	 * the first slice start angle
	 */
	private static final SettingKey<String> START_ANGLE = new SettingKey<String>(
			"startAngle", true);
	/**
	 * the first slice start angle
	 */
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>(
			"show");

	public Integer getLineWidth() {
		return get(LINE_WIDTH);
	}

	/**
	 * @param lineWidth
	 *          the line width in pixels
	 */
	public PieChartSettings setLineWidth(Integer lineWidth) {
		set(LINE_WIDTH, lineWidth);
		return this;
	}

	public Boolean getFill() {
		return get(FILL);
	}

	/**
	 * @param fill
	 *          true to fill the area from the line to the x axis, false for
	 *          (transparent) no fill
	 */
	public PieChartSettings setFill(Boolean fill) {
		set(FILL, fill);
		return this;
	}

	public String getFillColor() {
		return get(FILL_COLOR);
	}

	/**
	 * @param fillColor
	 *          fill color
	 */
	public PieChartSettings setFillColor(String fillColor) {
		set(FILL_COLOR, fillColor);
		if (fillColor != null) {
			setFill(true);
		}
		return this;
	}

	public Float getFillOpacity() {
		return get(FILL_OPACITY);
	}

	/**
	 * @param fillOpacity
	 *          opacity of the fill color, set to 1 for a solid fill, 0 hides the
	 *          fill
	 */
	public PieChartSettings setFillOpacity(Float fillOpacity) {
		set(FILL_OPACITY, fillOpacity);
		if (fillOpacity != null) {
			setFill(true);
		}
		return this;
	}

	public Integer getExplode() {
		return get(EXPLODE);
	}

	/**
	 * @param explode
	 *          the number of pixels the splices will be far from the center
	 */
	public PieChartSettings setExplode(Integer explode) {
		set(EXPLODE, explode);
		return this;
	}

	public Float getSizeRatio() {
		return get(SIZE_RATIO);
	}

	/**
	 * @param sizeRatio
	 *          the size ratio of the pie relative to the plot
	 */
	public PieChartSettings setSizeRatio(Float sizeRatio) {
		set(SIZE_RATIO, sizeRatio);
		return this;
	}

	public String getStartAngle() {
		return get(START_ANGLE);
	}

	/**
	 * @param startAngle
	 *          the first slice start angle (e.g. <code>(Math.PI/2 * 0.8)</code> )
	 */
	public PieChartSettings setStartAngle(String startAngle) {
		set(START_ANGLE, startAngle);
		return this;
	}
}
