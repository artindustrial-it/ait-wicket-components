package com.ait.wicket.component.chart;

public class TrendlineUtil {

	public static FloatLine getTrendline(FloatLine data) {
		return getTrendline("Trend " + data.getLabel(), data);
	}

	public static FloatLine getTrendline(String label, FloatLine data) {
		Float[][] xAndYAxis = convertToArray(data);
		Trendline trendline = new Trendline(xAndYAxis[0], xAndYAxis[1]);
		FloatCoordinates[] points = data.getPoints();
		return new FloatLine("Trend " + data.getLabel(), new FloatCoordinates[] {
				new FloatCoordinates(points[0].getX(), trendline.getStart()),
				new FloatCoordinates(points[points.length - 1].getX(),
						trendline.getEnd()) });
	}

	public static Timeline getTrendline(Timeline data) {
		return getTrendline("Trend " + data.getLabel(), data);
	}

	public static Timeline getTrendline(String label, Timeline data) {
		Float[][] xAndYAxis = convertToArray(data);
		Trendline trendline = new Trendline(xAndYAxis[0], xAndYAxis[1]);
		TimeCoordinates[] points = data.getPoints();
		return new Timeline("Trend " + data.getLabel(), new TimeCoordinates[] {
				new TimeCoordinates(points[0].getX(), trendline.getStart()),
				new TimeCoordinates(points[points.length - 1].getX(),
						trendline.getEnd()) });
	}

	private static Float[][] convertToArray(Timeline data) {
		TimeCoordinates[] points = data.getPoints();
		Float[][] result = new Float[][] { new Float[points.length],
				new Float[points.length] };
		for (int i = 0; i < points.length; i++) {
			result[0][i] = (float) i + 1;
			result[1][i] = points[i].getY();
		}
		return result;
	}

	private static Float[][] convertToArray(FloatLine data) {
		FloatCoordinates[] points = data.getPoints();
		Float[][] result = new Float[][] { new Float[points.length],
				new Float[points.length] };
		for (int i = 0; i < points.length; i++) {
			result[0][i] = points[i].getX();
			result[1][i] = points[i].getY();
		}
		return result;
	}

	public static class Trendline {
		private Float[] xAxisValues;
		private Float[] yAxisValues;
		private int count;
		private float xAxisValuesSum;
		private float yAxisValuesSum;
		private int xxSum;
		private int xySum;
		private float slope;
		private float intercept;
		private float start;
		private float end;

		public Trendline(Float[] xAxisValues2, Float[] yAxisValues2) {
			this.xAxisValues = xAxisValues2;
			this.yAxisValues = yAxisValues2;
			this.Initialize();
		}

		private void Initialize() {
			this.count = this.yAxisValues.length;
			this.yAxisValuesSum = sum(this.yAxisValues);
			this.xAxisValuesSum = sum(this.xAxisValues);
			this.xxSum = 0;
			this.xySum = 0;

			for (int i = 0; i < this.count; i++) {
				this.xySum += (this.xAxisValues[i] * this.yAxisValues[i]);
				this.xxSum += (this.xAxisValues[i] * this.xAxisValues[i]);
			}

			this.slope = calculateSlope();
			this.intercept = this.calculateIntercept();
			this.start = this.calculateStart();
			this.end = this.calculateEnd();
		}

		public float getStart() {
			return start;
		}

		public float getEnd() {
			return end;
		}

		private float calculateSlope() {
			try {
				return ((this.count * this.xySum) - (this.xAxisValuesSum * this.yAxisValuesSum))
						/ ((this.count * this.xxSum) - (this.xAxisValuesSum * this.xAxisValuesSum));
			} catch (ArithmeticException e) {
				return 0;
			}
		}

		private float calculateIntercept() {
			return (this.yAxisValuesSum - (slope * this.xAxisValuesSum)) / this.count;
		}

		private float calculateStart() {
			return (slope * xAxisValues[0]) + intercept;
		}

		private float calculateEnd() {
			return (slope * xAxisValues[count - 1]) + intercept;
		}

		private float sum(Float... list) {
			float sum = 0;
			for (Float item : list) {
				if (item == null) {
					throw new IllegalArgumentException(
							"Float für Trendlinie darf nicht null sein");
				}
				sum += item;
			}
			return sum;
		}
	}
}
