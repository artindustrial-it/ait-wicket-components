package com.ait.wicket.component.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.wicket.model.IModel;

import com.ait.date.DateUtils;
import com.ait.wicket.component.chart.option.OptionSettings;
import com.ait.wicket.component.chart.option.XaxisSettings;
import com.ait.wicket.component.chart.option.XaxisSettings.Mode;

/**
 * Das Timeline Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see AbstractLineChartPanel
 * @author Manuel
 */
public class TimelineChartPanel extends
		AbstractLineChartPanel<TimelineChartData, Date, Float> {
	private static final long serialVersionUID = 1L;

	public TimelineChartPanel(String id, IModel<TimelineChartData> model) {
		super(id, model);
	}

	public TimelineChartPanel(String id, IModel<TimelineChartData> model,
			OptionSettings... options) {
		super(id, model, options);
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		List<OptionSettings> options = new ArrayList<OptionSettings>(Arrays.asList(super.getDefaultOptions()));
		options.add(new XaxisSettings().setMode(Mode.TIME).setAutoscale(true));
		return options.toArray(new OptionSettings[options.size()]);
	}

	@Override
	protected String convertX(Date xAxisDate) {
		XaxisSettings xAxisSetting = findSetting(XaxisSettings.class);
		if (xAxisSetting == null) {
			xAxisSetting = new XaxisSettings();
			addSettings(xAxisSetting);
		}
		String dateString = null;
		dateString = DateUtils.getFormattedDate(xAxisDate, "yyyy/MM/dd hh:mm:ss");
//		TimeUnit timeUnit = xAxisSetting.getTimeUnit();
//		switch (timeUnit) {
//
//		case MILLISECOND:
//			return DateUtils.getFormattedDate(xAxisDate, "ssSSSS");
//		case SECOND:
//			return DateUtils.getFormattedDate(xAxisDate, "ss");
//		case MINUTE:
//			return DateUtils.getFormattedDate(xAxisDate, "hhmm");
//		case HOUR:
//			dateString = DateUtils.getFormattedDate(xAxisDate, "MM/dd/yyyy hh:mi:ss.SSSS");
//			break;
//		case DAY:
//		case MONTH:
//		case YEAR:
//			// is default format
//		default:
//			xAxisSetting.setTimeUnit(TimeUnit.YEAR);
//			break;
//		}
		if (dateString != null) {
			return "new Date(\"" + dateString + "\").getTime()";
		}
		return null;
	}

	@Override
	protected String convertY(Float y) {
		return y.toString();
	}

}
