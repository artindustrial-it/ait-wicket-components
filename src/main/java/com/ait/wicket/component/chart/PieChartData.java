package com.ait.wicket.component.chart;

import java.io.Serializable;

public class PieChartData implements Serializable {
	private static final long serialVersionUID = 1L;
	Slice[] slices;
	
	public PieChartData(Slice[] slices) {
		this.slices = slices;
	}
}
