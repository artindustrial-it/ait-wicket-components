package com.ait.wicket.component.chart;

import java.io.Serializable;

import org.apache.wicket.Component;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import com.ait.wicket.component.chart.option.OptionSettings;
import com.ait.wicket.component.chart.option.RootSettings;
import com.ait.wicket.component.generics.TypedPanel;

/**
 * Um dieses ChartPanel zu verwenden, übergibt man ihm ein {@link IModel} auf
 * das entsprechende Objekt. <br/>
 * Um z.B. Filter einzubauen, braucht man nur die {@link IModel#getObject()}
 * Methode zu überschreiben und entsprechend gefilterte Daten zurück geben, die
 * Aktualisierung geschieht dann automatisch.
 * 
 * @author Manuel
 * 
 * @param <T>
 */
public abstract class BaseChartPanel<T extends Serializable> extends
		TypedPanel<T> {
	private static final long serialVersionUID = 1L;

	public static final JavaScriptResourceReference FLOTR2_JS = new JavaScriptResourceReference(
			BaseChartPanel.class, "/resources/flotr2.min.js");
	public static final JavaScriptResourceReference AIT_FLOTR2_CHARTS_JS = new JavaScriptResourceReference(
			BaseChartPanel.class, "/resources/ait-charts.js");
	private WebMarkupContainer chartContainer;
	private RootSettings rootSettings;

	public BaseChartPanel(String id, IModel<T> model) {
		super(id, model);
		// initialize settings
		clearSettings();
		addSettings(getDefaultOptions());
	}

	public BaseChartPanel(String id, IModel<T> model, OptionSettings... options) {
		this(id, model);
		if (options != null && options.length > 0) {
			addSettings(options);
		}
	}

	protected abstract OptionSettings[] getDefaultOptions();

	@Override
	public void renderHead(IHeaderResponse response) {
		response.render(JavaScriptReferenceHeaderItem.forReference(FLOTR2_JS));
		response.render(JavaScriptReferenceHeaderItem
				.forReference(AIT_FLOTR2_CHARTS_JS));
	}

	/**
	 * Only override, if you want to do custom stuff, e.g. print more than this
	 * class supports.<br/>
	 * You can add a title, add a custom filter and add one chart.
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(getChartContainer("container.chart"));
		add(getChart("js.chart"));
	}

	protected WebMarkupContainer getChartContainer(String id) {
		chartContainer = new WebMarkupContainer(id);
		chartContainer.setOutputMarkupId(true);
		return chartContainer;
	}

	/**
	 * This method creates a {@link Model} which prints the Flotr2 js chart
	 * function into a script-Tag.<br/>
	 * Therefor it calls {@link BaseChartPanel#convertDataToString(Serializable)}
	 * and {@link BaseChartPanel#getOptionsString()}.
	 * 
	 * @see <a
	 *      href="http://www.humblesoftware.com/flotr2/documentation">flotr2 Documentation</a>
	 * @param id
	 * @return
	 */
	protected Component getChart(String id) {
		Model<String> chartModel = new Model<String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {
				if (getModelObject() != null) {
					return "Flotr.draw(document.getElementById('" + getChartId() + "'), "
							+ BaseChartPanel.this.convertDataToString(getModelObject())
							+ ", " + getOptionsString() + ");";
				}
				return "";
			}
		};
		Label chart = new Label(id, chartModel);
		chart.setEscapeModelStrings(false);
		return chart;
	}

	public String getChartId() {
		return chartContainer.getMarkupId(true);
	}

	/**
	 * Converts the chart data into a js array, which is put into the Flotr2
	 * chart. This method is called from {@link BaseChartPanel#getChart(String)}.
	 * 
	 * @see BaseChartPanel#getChart(String)
	 * @param object
	 * @return
	 */
	protected abstract String convertDataToString(T object);

	/**
	 * This method converts all {@link ChartOption}s into a {@link String} for
	 * further convinient useage.<br/>
	 * 
	 * @return all options in flotr2 format or null
	 */
	private String getOptionsString() {
		String flotrOptions = rootSettings.toFlotrString();
		StringBuilder sb = new StringBuilder();
		if (flotrOptions != null && flotrOptions.length() > 0) {
			sb.append("{");
			sb.append(flotrOptions);
			// remove last ','
			if (sb.charAt(sb.length() - 1) == '\'') {
				sb.replace(sb.length() - 2, sb.length() - 1, "");
			}
			sb.append("}");
		}
		return sb.toString();
	}

	public BaseChartPanel<T> clearSettings() {
		rootSettings = new RootSettings();
		return this;
	}
	
	public <E extends OptionSettings> E findSetting(Class<E> settings) {
		if (settings != null) {
			return rootSettings.findSetting(settings);
		}
		return null;
	}

	public BaseChartPanel<T> addSettings(OptionSettings... settings) {
		if (settings != null && settings.length > 0) {
			for (OptionSettings item : settings) {
				rootSettings.mergeSettings(item);
			}
		}
		return this;
	}
}
