package com.ait.wicket.component.chart.option;



/**
 * @author Manuel
 */
public class Y2AxisSettings extends AbstractAxisSettings {

	public Y2AxisSettings() {
	}

	@Override
	public String getName() {
		return "y2axis";
	}
}
