package com.ait.wicket.component.chart;

import org.apache.wicket.model.IModel;

import com.ait.wicket.component.chart.option.BarChartSettings;
import com.ait.wicket.component.chart.option.GridSettings;
import com.ait.wicket.component.chart.option.OptionSettings;
import com.ait.wicket.component.chart.option.XaxisSettings;
import com.ait.wicket.component.chart.option.YaxisSettings;

/**
 * Das Bar Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see AbstractLineChartPanel
 * @author Manuel
 */
public class BarChartPanel extends BaseChartPanel<BarChartData> {
	private static final long serialVersionUID = 1L;

	public BarChartPanel(String id, IModel<BarChartData> model) {
		super(id, model);
	}

	public BarChartPanel(String id, IModel<BarChartData> model,
			OptionSettings... options) {
		super(id, model, options);
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] { new BarChartSettings(),
				new XaxisSettings().setAutoscale(true).setShowLabels(true),
				new YaxisSettings().setShowLabels(true),
				new GridSettings().setMinorVerticalLines(false) };
	}

	@Override
	protected String convertDataToString(BarChartData filteredData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.getBarSequence().length > 0
				&& filteredData.getBarSequence()[0].getBars().length > 0) {
			// add all lines
			int lineCount = 0;
			for (BarSequence barSequence : filteredData.getBarSequence()) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				for (Bar bar : barSequence.getBars()) {
					sb.append("[" + bar.getX() + "," + bar.getY() + "],");
				}
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(barSequence.getLabel());
				sb.append("'");
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}
}
