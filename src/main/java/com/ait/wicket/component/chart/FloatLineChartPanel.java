package com.ait.wicket.component.chart;

import org.apache.wicket.model.IModel;

import com.ait.wicket.component.chart.option.OptionSettings;

/**
 * Das Line Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see BaseChartPanel
 * @author Manuel
 */
public class FloatLineChartPanel extends 
	AbstractLineChartPanel<FloatLineChartData, Float, Float> {
	private static final long serialVersionUID = 1L;

	public FloatLineChartPanel(String id, IModel<FloatLineChartData> model) {
		super(id, model);
	}

	public FloatLineChartPanel(String id, IModel<FloatLineChartData> model,
			OptionSettings... options) {
		super(id, model, options);
	}

	protected String convertX(Float x) {
		return x != null ? x.toString() : "";
	}

	protected String convertY(Float y) {
		return y != null ? y.toString() : "";
	}
}
