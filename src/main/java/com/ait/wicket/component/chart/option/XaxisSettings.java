package com.ait.wicket.component.chart.option;

/**
 * @author Manuel
 */
public class XaxisSettings extends AbstractAxisSettings {

	public XaxisSettings() {
	}

	@Override
	public String getName() {
		return "xaxis";
	}

	private static final SettingKey<Mode> MODE = new SettingKey<Mode>("mode");
	private static final SettingKey<String> TIME_FORMAT = new SettingKey<String>(
			"timeFormat");
	private static final SettingKey<TimeMode> TIME_MODE = new SettingKey<TimeMode>(
			"timeMode");
	private static final SettingKey<TimeUnit> TIME_UNIT = new SettingKey<TimeUnit>(
			"timeUnit");

	public Mode getMode() {
		return get(MODE);
	}

	/**
	 * @param mode
	 *          can be 'time' or 'normal'
	 */
	public XaxisSettings setMode(Mode mode) {
		set(MODE, mode);
		return this;
	}

	public String getTimeFormat() {
		return get(TIME_FORMAT);
	}

	/**
	 * @param timeFormat
	 */
	public XaxisSettings setTimeFormat(String timeFormat) {
		set(TIME_FORMAT, timeFormat);
		if (timeFormat != null) {
			set(MODE, Mode.TIME);
		}
		return this;
	}

	public TimeMode getTimeMode() {
		return get(TIME_MODE);
	}

	/**
	 * @param timeMode
	 *          'UTC' for UTC time, 'local' for local time
	 */
	public XaxisSettings setTimeMode(TimeMode timeMode) {
		set(TIME_MODE, timeMode);
		if (timeMode != null) {
			set(MODE, Mode.TIME);
		}
		return this;
	}

	public TimeUnit getTimeUnit() {
		return get(TIME_UNIT);
	}

	/**
	 * @param timeUnit
	 *          Unit for time (millisecond, second, minute, hour, day, month,
	 *          year)
	 */
	public XaxisSettings setTimeUnit(TimeUnit timeUnit) {
		set(TIME_UNIT, timeUnit);
		if (timeUnit != null) {
			set(MODE, Mode.TIME);
			switch (timeUnit) {
  			case MILLISECOND:
  			case MINUTE:
  			case HOUR:
  				// TODO MM move the 2 default JS function here?
  				// function(x) {
  				// var
  				// x = parseInt(x),
  				// months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
  				// 'Oct', 'Nov', 'Dec'];
  				// return months[(x - 1) % 12];
  				// }

//  				setTickFormatter("getFormattedTime");
  				break;
				case DAY:
				case MONTH:
				case YEAR:
				default:
//					setTickFormatter("getFormattedDate");
					break;
			}
		}
		return this;
	}

	@Override
	protected String convertSettingValueToFlotrString(SettingKey<?> key,
			Object value) {
		if (key == MODE || key == TIME_MODE || key == TIME_UNIT) {
			value = value.toString();
		}
		String result = super.convertSettingValueToFlotrString(key, value);
		return result;
	}

	public static enum Mode {
		NORMAL, TIME;
		public String toString() {
			return name().toLowerCase();
		};
	}

	public static enum TimeMode {
		UTC, LOCAL;
		public String toString() {
			if (this == UTC) {
				return name();
			} else {
				return name().toLowerCase();
			}
		};
	}

	public static enum TimeUnit {
		MILLISECOND, SECOND, MINUTE, HOUR, DAY, MONTH, YEAR;
		public String toString() {
			return name().toLowerCase();
		};
	}
}
