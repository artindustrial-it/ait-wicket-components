package com.ait.wicket.component.chart;

import org.apache.wicket.model.IModel;

import com.ait.wicket.component.chart.option.GridSettings;
import com.ait.wicket.component.chart.option.OptionSettings;
import com.ait.wicket.component.chart.option.PieChartSettings;
import com.ait.wicket.component.chart.option.XaxisSettings;
import com.ait.wicket.component.chart.option.YaxisSettings;

/**
 * Das Pie Chart nimmt ein {@link IModel}<{@link PieChartData}>.
 * 
 * @see BaseChartPanel
 * @author Manuel
 */
public class PieChartPanel extends BaseChartPanel<PieChartData> {
	private static final long serialVersionUID = 1L;

	public PieChartPanel(String id, IModel<PieChartData> model) {
		super(id, model);
	}

	public PieChartPanel(String id, IModel<PieChartData> model,
			OptionSettings... options) {
		super(id, model, options);
	}

	@Override
	protected String convertDataToString(PieChartData filteredData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.slices.length > 0) {
			// add all lines
			int lineCount = 0;
			for (Slice slice : filteredData.slices) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				sb.append("[0," + slice.value + "]");
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(slice.label);
				sb.append("'");
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] {
				new PieChartSettings().setExplode(6),
				new XaxisSettings().setShowLabels(false),
				new YaxisSettings().setShowLabels(false),
				new GridSettings().setVerticalLines(false).setHorizontalLines(false)
		};
	}
}
