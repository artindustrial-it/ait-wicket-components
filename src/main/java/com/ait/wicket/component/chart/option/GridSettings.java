package com.ait.wicket.component.chart.option;

public class GridSettings extends AbstractSettings {
	private static final SettingKey<String> COLOR = new SettingKey<String>(
			"color");
	private final SettingKey<String> BACKGROUND_COLOR = new SettingKey<String>(
			"backgroundColor") {
		public boolean isFunction() {
			String value = getBackgroundColor();
			return value.toString().startsWith("{") && value.toString().endsWith("}");
		};
	};
	private static final SettingKey<String> BACKGROUND_IMAGE = new SettingKey<String>(
			"backgroundImage");
	private static final SettingKey<Boolean> VERTICAL_LINES = new SettingKey<Boolean>(
			"verticalLines");
	private static final SettingKey<Boolean> MINOR_VERTICAL_LINES = new SettingKey<Boolean>(
			"minorVerticalLines");
	private static final SettingKey<Boolean> HORIZONTAL_LINES = new SettingKey<Boolean>(
			"horizontalLines");
	private static final SettingKey<Boolean> MINOR_HORIZONTAL_LINES = new SettingKey<Boolean>(
			"minorHorizontalLines");
	private static final SettingKey<Float> WATERMARK_ALPHA = new SettingKey<Float>(
			"watermarkAlpha");
	private static final SettingKey<String> TICK_COLOR = new SettingKey<String>(
			"tickColor");
	private static final SettingKey<Integer> LABEL_MARGIN = new SettingKey<Integer>(
			"labelMargin");
	private static final SettingKey<Integer> OUTLINE_WIDTH = new SettingKey<Integer>(
			"outlineWidth");
	private static final SettingKey<String> OUTLINE = new SettingKey<String>(
			"outline");
	private static final SettingKey<Boolean> CIRCULAR = new SettingKey<Boolean>(
			"circular");
	boolean isBackgroundFunction;

	public GridSettings() {
	}

	@Override
	public String getName() {
		return "grid";
	}

	public String getColor() {
		return super.get(COLOR);
	}

	/**
	 * @param color
	 *          primary color used for outline and labels, null for default value
	 * @return
	 */
	public GridSettings setColor(String color) {
		super.set(COLOR, color);
		return this;
	}

	public String getBackgroundColor() {
		return super.get(BACKGROUND_COLOR);
	}

	/**
	 * You can give a background color here, or a function in
	 * {@link GridSettings#setBackgroundFunction(String)}.
	 * 
	 * @param backgroundColor
	 *          null for transparent, else color
	 * @return
	 */
	public GridSettings setBackgroundColor(String backgroundColor) {
		super.set(BACKGROUND_COLOR, backgroundColor);
		isBackgroundFunction = false;
		return this;
	}

	/**
	 * Sets a function as background color, like
	 * 
	 * <pre>
	 * {@code
	 * colors: [[0, '#fff'], [1, '#eee']], 
	 * start: 'top', 
	 * end: 'bottom'
	 * }
	 * </pre>
	 * 
	 * for a gradient from light gray to white.
	 * 
	 * @param backgroundFunction
	 * @return
	 */
	public GridSettings setBackgroundFunction(String backgroundFunction) {
		super.set(BACKGROUND_COLOR, backgroundFunction);
		isBackgroundFunction = true;
		return this;
	}

	public String getBackgroundImage() {
		return super.get(BACKGROUND_IMAGE);
	}

	/**
	 * @param backgroundImage
	 *          background image src
	 * @return
	 */
	public GridSettings setBackgroundImage(String backgroundImage) {
		super.set(BACKGROUND_IMAGE, backgroundImage);
		return this;
	}

	public Boolean getVerticalLines() {
		return super.get(VERTICAL_LINES);
	}

	/**
	 * @param verticalLines
	 *          whether to show gridlines in vertical direction
	 * @return
	 */
	public GridSettings setVerticalLines(Boolean verticalLines) {
		super.set(VERTICAL_LINES, verticalLines);
		return this;
	}

	public Boolean getHorizontalLines() {
		return super.get(HORIZONTAL_LINES);
	}

	/**
	 * @param horizontalLines
	 *          whether to show gridlines in horizontal direction
	 * @return
	 */
	public GridSettings setHorizontalLines(Boolean horizontalLines) {
		super.set(HORIZONTAL_LINES, horizontalLines);
		return this;
	}

	public String getTickColor() {
		return super.get(TICK_COLOR);
	}

	/**
	 * @param tickColor
	 *          color used for the ticks
	 * @return
	 */
	public GridSettings setTickColor(String tickColor) {
		super.set(TICK_COLOR, tickColor);
		return this;
	}

	public Integer getLabelMargin() {
		return super.get(LABEL_MARGIN);
	}

	/**
	 * @param labelMargin
	 *          margin in pixels
	 */
	public GridSettings setLabelMargin(Integer labelMargin) {
		super.set(LABEL_MARGIN, labelMargin);
		return this;
	}

	public Float getWatermarkAlpha() {
		return super.get(WATERMARK_ALPHA);
	}

	/**
	 * @param watermarkAlpha
	 */
	public GridSettings setWatermarkAlpha(Float watermarkAlpha) {
		super.set(WATERMARK_ALPHA, watermarkAlpha);
		return this;
	}

	public Integer getOutlineWidth() {
		return super.get(OUTLINE_WIDTH);
	}

	/**
	 * @param outlineWidth
	 *          width of the grid outline/border in pixels
	 */
	public GridSettings setOutlineWidth(Integer outlineWidth) {
		super.set(OUTLINE_WIDTH, outlineWidth);
		return this;
	}

	public String getOutline() {
		return super.get(OUTLINE);
	}

	/**
	 * @param outline
	 *          walls of the outline to display, e.g. nsew
	 */
	public GridSettings setOutline(String outline) {
		super.set(OUTLINE, outline);
		return this;
	}

	public Boolean getCircular() {
		return super.get(CIRCULAR);
	}

	/**
	 * @param circular
	 *          if set to true, the grid will be circular, must be used when
	 *          radars are drawn
	 */
	public GridSettings setCircular(Boolean circular) {
		super.set(CIRCULAR, circular);
		return this;
	}

	public Boolean getMinorVerticalLines() {
		return super.get(MINOR_VERTICAL_LINES);
	}

	/**
	 * @param minorVerticalLines
	 *          whether to show gridlines for minor ticks in vertical direction
	 */
	public GridSettings setMinorVerticalLines(Boolean minorVerticalLines) {
		super.set(MINOR_VERTICAL_LINES, minorVerticalLines);
		return this;
	}

	public Boolean getMinorHorizontalLines() {
		return super.get(MINOR_HORIZONTAL_LINES);
	}

	/**
	 * @param minorHorizontalLines
	 *          whether to show gridlines for minor ticks in horizontal direction
	 */
	public GridSettings setMinorHorizontalLines(Boolean minorHorizontalLines) {
		super.set(MINOR_HORIZONTAL_LINES, minorHorizontalLines);
		return this;
	}
}
