package com.ait.wicket.component.generics;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Das {@link TypedPanel} ist die Erweiterung eines normalen {@link Panel}s um
 * Generics (z.B. {@link TypedPanel#getModelObject()}).
 * 
 * @author Manuel
 */
public class TypedPanel<T> extends Panel {
	private static final long serialVersionUID = 1L;

	public TypedPanel(final String id) {
		super(id);
	}

	public TypedPanel(final String id, final IModel<? extends T> model) {
		super(id, model);
	}

	public final T getModelObject() {
		return getModel() != null ? getModel().getObject() : null;
	}

	public final void setModelObject(final T object) {
		setDefaultModelObject(object);
	}

	public final void setModel(final IModel<T> model) {
		setDefaultModel(model);
	}

	@SuppressWarnings("unchecked")
	public final IModel<T> getModel() {
		return (IModel<T>) getDefaultModel();
	}
}
