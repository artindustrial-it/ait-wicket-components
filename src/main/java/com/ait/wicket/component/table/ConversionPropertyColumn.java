package com.ait.wicket.component.table;

import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.convert.IConverter;

import com.ait.wicket.model.ConversionModel;

/**
 *
 * @author Manuel
 *
 * @param <T>
 *            the domain object used in the table
 * @param <S>
 *            the output format, usually String
 * @param <R>
 *            the object, from which we will convert to <S>.
 */
public class ConversionPropertyColumn<T, S, R> extends PropertyColumn<T, S> {

	private IConverter<R> converter;

	public ConversionPropertyColumn(final IModel<String> displayModel,
			final String propertyExpression) {
		super(displayModel, propertyExpression);
	}

	public ConversionPropertyColumn(final IModel<String> displayModel,
			final S sortProperty, final String propertyExpression) {
		super(displayModel, sortProperty, propertyExpression);
	}

	public ConversionPropertyColumn(final IModel<String> displayModel,
			final String propertyExpression, final IConverter<R> converter) {
		this(displayModel, propertyExpression);
		this.converter = converter;
	}

	public ConversionPropertyColumn(final IModel<String> displayModel,
			final S sortProperty, final String propertyExpression,
			final IConverter<R> converter) {
		this(displayModel, sortProperty, propertyExpression);
		this.converter = converter;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IModel<Object> getDataModel(final IModel<T> rowModel) {
		IModel<Object> result = super.getDataModel(rowModel);
		if (converter != null) {
			result = new ConversionModel(result, converter);
		}
		return result;
	}

}
