package com.ait.wicket.component.wizard;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.wizard.IWizard;
import org.apache.wicket.extensions.wizard.dynamic.IDynamicWizardStep;
import org.apache.wicket.model.IModel;

public abstract class TypedDynamicWizardStep<T> extends ResponsiveWizardStep {

	/**
	 * Construct without a title and a summary. Useful for when you provide a
	 * custom header by overriding
	 * {@link #getHeader(String, Component, IWizard)}.
	 * 
	 * @param previousStep
	 *            The previous step. May be null if this is the first step in
	 *            the wizard
	 */
	public TypedDynamicWizardStep(final IDynamicWizardStep previousStep) {
		super(previousStep);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is
	 * active.
	 * 
	 * @param previousStep
	 *            The previous step. May be null if this is the first step in
	 *            the wizard
	 * @param title
	 *            the title of this step.
	 * @param summary
	 *            a brief summary of this step or some usage guidelines.
	 */
	public TypedDynamicWizardStep(final IDynamicWizardStep previousStep,
			final IModel<String> title, final IModel<String> summary) {
		super(previousStep, title, summary);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is
	 * active.
	 * 
	 * @param previousStep
	 *            The previous step. May be null if this is the first step in
	 *            the wizard
	 * @param title
	 *            the title of this step.
	 * @param summary
	 *            a brief summary of this step or some usage guidelines.
	 * @param model
	 *            Any model which is to be used for this step
	 */
	public TypedDynamicWizardStep(final IDynamicWizardStep previousStep,
			final IModel<String> title, final IModel<String> summary,
			final IModel<?> model) {
		super(previousStep, title, summary, model);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is
	 * active.
	 * 
	 * @param previousStep
	 *            The previous step. May be null if this is the first step in
	 *            the wizard
	 * @param title
	 *            the title of this step.
	 * @param summary
	 *            a brief summary of this step or some usage guidelines.
	 */
	public TypedDynamicWizardStep(final IDynamicWizardStep previousStep,
			final String title, final String summary) {
		super(previousStep, title, summary);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is
	 * active.
	 * 
	 * @param previousStep
	 *            The previous step. May be null if this is the first step in
	 *            the wizard
	 * @param title
	 *            the title of this step.
	 * @param summary
	 *            a brief summary of this step or some usage guidelines.
	 * @param model
	 *            Any model which is to be used for this step
	 */
	public TypedDynamicWizardStep(final IDynamicWizardStep previousStep,
			final String title, final String summary, final IModel<?> model) {
		super(previousStep, title, summary, model);
	}

	public final T getModelObject() {
		return getModel().getObject();
	}

	public final void setModelObject(final T object) {
		setDefaultModelObject(object);
	}

	public final void setModel(final IModel<T> model) {
		setDefaultModel(model);
	}

	@SuppressWarnings("unchecked")
	public final IModel<T> getModel() {
		return (IModel<T>) getDefaultModel();
	}
}
