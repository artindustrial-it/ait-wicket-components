package com.ait.wicket.component.wizard;

import org.apache.wicket.extensions.wizard.IWizard;
import org.apache.wicket.extensions.wizard.WizardButtonBar;

import com.ait.wicket.behavior.grid.BootstrapColumn;
import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.behavior.grid.BootstrapLayout.Type;
import com.ait.wicket.util.BootstrapGridUtils;

public class ResponsiveWizardButtonBar extends WizardButtonBar {

	public ResponsiveWizardButtonBar(final String id, final IWizard wizard) {
		super(id, wizard);
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		BootstrapGridUtils.addGridLayoutToChildren(this, new BootstrapColumn(
				Grid.GRID_3, Type.TABLET), new BootstrapColumn(Grid.GRID_6,
				Type.PHONE));
	}
}
