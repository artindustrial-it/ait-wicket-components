package com.ait.wicket.component.wizard;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.wizard.IWizardModel;
import org.apache.wicket.extensions.wizard.Wizard;

import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.util.BootstrapGridUtils;

public class ResponsiveWizard extends Wizard {

	/**
	 * Construct. Adds the default style.
	 * <p>
	 * If you override this class, it makes sense to call this constructor
	 * (super(id)), then - in your constructor - construct a transition model
	 * and then call {@link #init(IWizardModel)} to initialize the wizard.
	 * </p>
	 * <p>
	 * This constructor is not meant for normal clients of this class
	 * </p>
	 * 
	 * @param id
	 *            The component model
	 */
	public ResponsiveWizard(final String id) {
		super(id, true);
	}

	/**
	 * Construct.
	 * <p>
	 * If you override this class, it makes sense to call this constructor
	 * (super(id)), then - in your constructor - construct a transition model
	 * and then call {@link #init(IWizardModel)} to initialize the wizard.
	 * </p>
	 * <p>
	 * This constructor is not meant for normal clients of this class
	 * </p>
	 * 
	 * @param id
	 *            The component model
	 * @param addDefaultCssStyle
	 *            Whether to add the {@link #addDefaultCssStyle() default style}
	 */
	public ResponsiveWizard(final String id, final boolean addDefaultCssStyle) {
		super(id, addDefaultCssStyle);
	}

	/**
	 * Construct with a transition model. Adds the default style.
	 * <p>
	 * For most clients, this is typically the right constructor to use.
	 * </p>
	 * 
	 * @param id
	 *            The component id
	 * @param wizardModel
	 *            The transitions model
	 */
	public ResponsiveWizard(final String id, final IWizardModel wizardModel) {
		super(id, wizardModel, true);
	}

	/**
	 * Construct with a transition model.
	 * <p>
	 * For most clients, this is typically the right constructor to use.
	 * </p>
	 * 
	 * @param id
	 *            The component id
	 * @param wizardModel
	 *            The transitions model
	 * @param addDefaultCssStyle
	 *            Whether to add the {@link #addDefaultCssStyle() default style}
	 */
	public ResponsiveWizard(final String id, final IWizardModel wizardModel,
			final boolean addDefaultCssStyle) {
		super(id, wizardModel, addDefaultCssStyle);
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		BootstrapGridUtils.addGridLayoutToChildren(this, Grid.GRID_12);
	}

	@Override
	protected Component newButtonBar(final String id) {
		return new ResponsiveWizardButtonBar(id, this);
	}
}
