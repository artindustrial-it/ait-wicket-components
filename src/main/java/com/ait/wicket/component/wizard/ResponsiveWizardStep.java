package com.ait.wicket.component.wizard;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.wizard.IWizard;
import org.apache.wicket.extensions.wizard.IWizardStep;
import org.apache.wicket.extensions.wizard.WizardStep;
import org.apache.wicket.extensions.wizard.dynamic.DynamicWizardStep;
import org.apache.wicket.extensions.wizard.dynamic.IDynamicWizardStep;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import com.ait.wicket.behavior.grid.BootstrapColumn;
import com.ait.wicket.behavior.grid.BootstrapGridLayoutBehavior;
import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.behavior.grid.BootstrapLayout.Type;
import com.ait.wicket.util.BootstrapGridUtils;

/**
 * a responsive implementation of {@link IWizardStep}. It is also a panel, which
 * is used as the view component.
 * 
 * @see WizardStep
 * 
 * @author Manuel Manhart
 */
public abstract class ResponsiveWizardStep extends DynamicWizardStep {
	private static final long serialVersionUID = 1L;

	/**
	 * responsive header for wizards.
	 */
	private final class ResponsiveHeader extends Panel {
		private static final long serialVersionUID = 1L;

		/**
		 * Construct.
		 * 
		 * @param id
		 *          The component id
		 * @param wizard
		 *          The containing wizard
		 */
		public ResponsiveHeader(final String id, final IWizard wizard) {
			super(id);
			setDefaultModel(new CompoundPropertyModel<IWizard>(wizard));
		}

		@Override
		protected void onInitialize() {
			super.onInitialize();

			Component title = new Label("title", new AbstractReadOnlyModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getObject() {
					return getTitle();
				}
			}).setEscapeModelStrings(false);
			if (BootstrapGridUtils.isOnBootstrapPage(this)) {
				title.add(BootstrapGridLayoutBehavior.of(new BootstrapColumn(
						Grid.GRID_12, Type.TABLET)));
			}
			add(title);
			Component summary = new Label("summary",
					new AbstractReadOnlyModel<String>() {
						private static final long serialVersionUID = 1L;

						@Override
						public String getObject() {
							return getSummary();
						}
					}).setEscapeModelStrings(false);
			summary.add(BootstrapGridLayoutBehavior.of(new BootstrapColumn(
					Grid.GRID_12)));
			add(summary);
		}
	}

	/**
	 * Construct without a title and a summary. Useful for when you provide a
	 * custom header by overriding {@link #getHeader(String, Component, IWizard)}.
	 * 
	 * @param previousStep
	 *          The previous step. May be null if this is the first step in the
	 *          wizard
	 */
	public ResponsiveWizardStep(final IDynamicWizardStep previousStep) {
		super(previousStep);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is active.
	 * 
	 * @param previousStep
	 *          The previous step. May be null if this is the first step in the
	 *          wizard
	 * @param title
	 *          the title of this step.
	 * @param summary
	 *          a brief summary of this step or some usage guidelines.
	 */
	public ResponsiveWizardStep(final IDynamicWizardStep previousStep,
			final IModel<String> title, final IModel<String> summary) {
		super(previousStep, title, summary);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is active.
	 * 
	 * @param previousStep
	 *          The previous step. May be null if this is the first step in the
	 *          wizard
	 * @param title
	 *          the title of this step.
	 * @param summary
	 *          a brief summary of this step or some usage guidelines.
	 * @param model
	 *          Any model which is to be used for this step
	 */
	public ResponsiveWizardStep(final IDynamicWizardStep previousStep,
			final IModel<String> title, final IModel<String> summary,
			final IModel<?> model) {
		super(previousStep, title, summary, model);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is active.
	 * 
	 * @param previousStep
	 *          The previous step. May be null if this is the first step in the
	 *          wizard
	 * @param title
	 *          the title of this step.
	 * @param summary
	 *          a brief summary of this step or some usage guidelines.
	 */
	public ResponsiveWizardStep(final IDynamicWizardStep previousStep,
			final String title, final String summary) {
		super(previousStep, title, summary);
	}

	/**
	 * Creates a new step with the specified title and summary. The title and
	 * summary are displayed in the wizard title block while this step is active.
	 * 
	 * @param previousStep
	 *          The previous step. May be null if this is the first step in the
	 *          wizard
	 * @param title
	 *          the title of this step.
	 * @param summary
	 *          a brief summary of this step or some usage guidelines.
	 * @param model
	 *          Any model which is to be used for this step
	 */
	public ResponsiveWizardStep(final IDynamicWizardStep previousStep,
			final String title, final String summary, final IModel<?> model) {
		super(previousStep, title, summary, model);
	}

	/**
	 * @see org.apache.wicket.extensions.wizard.IWizardStep#getHeader(java.lang.String,
	 *      org.apache.wicket.Component,
	 *      org.apache.wicket.extensions.wizard.IWizard)
	 */
	@Override
	public Component getHeader(final String id, final Component parent,
			final IWizard wizard) {
		return new ResponsiveHeader(id, wizard);
	}
}
