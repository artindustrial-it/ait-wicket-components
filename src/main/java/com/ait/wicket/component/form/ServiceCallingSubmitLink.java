package com.ait.wicket.component.form;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.SubmitLink;

import com.ait.wicket.component.i18n.I18nModel;

/**
 * 
 * This component makes it very easy when a form is submitted:<br/>
 * * to save via service call<br/>
 * * to reset the form model object (for a new transaction)<br/>
 * * to inform the user that his changes have been saved (via {@link ServiceCallingSubmitLink#DEFAULT_MESSAGE_KEY} or a
 * given property id)
 * 
 * Usage:
 * 
 * <pre>
 * add(new ServiceCallingSubmitLink(&quot;WicketIdOfMyLink&quot;, myServiceContributor, &quot;KeyOfMyFeedbackMessage&quot;,
 * 	myFormResettingComponent));
 * </pre>
 * 
 * @see ServiceContributor
 * @see FormResettingComponent
 * @see I18nModel
 * @author Manuel
 */
public class ServiceCallingSubmitLink extends SubmitLink {
	private static final long serialVersionUID = 1L;
	private FormResettingComponent formResettingComponent;
	private ServiceContributor serviceContributor;
	private final String messageKey;
	/**
	 * the default property for the message, the user gets when pressing a {@link ServiceCallingSubmitLink}
	 * 
	 * @returns action.accepted.info
	 */
	public final String DEFAULT_MESSAGE_KEY = "action.accepted.info";

	/**
	 * Initializes a {@link ServiceCallingSubmitLink}.<br/>
	 * Calls {@link ServiceCallingSubmitLink#ServiceCallingSubmitLink(String, ServiceContributor, String)}
	 * 
	 * @see ServiceContributor ServiceContributor - contributes the called
	 *      service method for saving the user action
	 * @see FormResettingComponent FormResettingComponent - resets the Form in a
	 *      Page / Panel
	 * @param id
	 * @param serviceContributor
	 * @param messageKey
	 */
	public ServiceCallingSubmitLink(final String id) {
		this(id, null, null);
	}

	/**
	 * Initializes a {@link ServiceCallingSubmitLink}.<br/>
	 * Calls {@link ServiceCallingSubmitLink#ServiceCallingSubmitLink(String, ServiceContributor, String)}
	 * 
	 * @see ServiceContributor ServiceContributor - contributes the called
	 *      service method for saving the user action
	 * @see FormResettingComponent FormResettingComponent - resets the Form in a
	 *      Page / Panel
	 * @param id
	 * @param serviceContributor
	 * @param messageKey
	 */
	public ServiceCallingSubmitLink(final String id, final ServiceContributor serviceContributor) {
		this(id, serviceContributor, null);
	}

	/**
	 * Initializes a {@link ServiceCallingSubmitLink}. Initializes the {@link FormResettingComponent} with the
	 * {@link ServiceContributor} if it
	 * also implements the {@link FormResettingComponent} interface. If not, no
	 * form reset will be called.<br/>
	 * Calls
	 * {@link ServiceCallingSubmitLink#ServiceCallingSubmitLink(String, ServiceContributor, String, FormResettingComponent)}
	 * 
	 * @see ServiceContributor ServiceContributor - contributes the called
	 *      service method for saving the user action
	 * @see FormResettingComponent FormResettingComponent - resets the Form in a
	 *      Page / Panel
	 * @param id
	 * @param serviceContributor
	 * @param messageKey
	 */
	public ServiceCallingSubmitLink(final String id, final ServiceContributor serviceContributor, final String messageKey) {
		this(id, serviceContributor, messageKey, null);
	}

	/**
	 * Initializes a {@link ServiceCallingSubmitLink}
	 * 
	 * @see ServiceContributor ServiceContributor - contributes the called
	 *      service method for saving the user action
	 * @see FormResettingComponent FormResettingComponent - resets the Form in a
	 *      Page / Panel
	 * @param id
	 * @param serviceContributor
	 *          if this argument is null or not passed, the ServiceContributor will be searched in the Hierarchy of the
	 *          Page
	 * @param messageKey
	 * @param formResettingComponent
	 */
	public ServiceCallingSubmitLink(final String id, final ServiceContributor serviceContributor,
		final String messageKey, final FormResettingComponent formResettingComponent) {
		super(id);
		this.serviceContributor = serviceContributor;
		this.messageKey = messageKey;
		this.formResettingComponent = formResettingComponent;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		if (serviceContributor == null) {
			serviceContributor = findServiceContributorInHierarchy();
		}
		if (formResettingComponent == null) {
			formResettingComponent = serviceContributor instanceof FormResettingComponent
				? (FormResettingComponent) serviceContributor : findFormResettingComponent();
		}
	}

	private FormResettingComponent findFormResettingComponent() {
		Component parent = getParent();
		while (parent != null) {
			if (parent instanceof FormResettingComponent) {
				return (FormResettingComponent) parent;
			}
			if ((parent instanceof WebPage)) {
				parent = null;
			} else {
				parent = parent.getParent();
			}
		}
		throw new IllegalArgumentException("Could not find any FormResettingComponent in Hierarchy of " +
			getPageRelativePath() + ".");
	}

	private ServiceContributor findServiceContributorInHierarchy() {
		Component parent = getParent();
		while (parent != null) {
			if (parent instanceof ServiceContributor) {
				return (ServiceContributor) parent;
			}
			if ((parent instanceof WebPage)) {
				parent = null;
			} else {
				parent = parent.getParent();
			}
		}
		throw new IllegalArgumentException("Could not find any ServiceContributor in Hierarchy of " +
			getPageRelativePath() + ".");
	}

	@Override
	public final void onSubmit() {
		super.onSubmit();
		// if service call went well, show a message if wanted
		if (serviceContributor.doServiceCallOnSave()) {
			// if the messageKey is null, try if we find any action with the
			// default key
			if (messageKey == null) {
				String resource = Application.get().getResourceSettings().getLocalizer()
					.getString(DEFAULT_MESSAGE_KEY, this, null, "");
				if (resource != null && resource.length() > 0) {
					info(resource);
//				} else if (WicketApplication.get().isDevelopmentMode()) {
//					info("DEVELOPMENT INFO ONLY: No message is printed on saving. Did not find anything under default key '" +
//						DEFAULT_MESSAGE_KEY + "'");
				}
			} else {
				info(I18nModel.getResource(messageKey));
			}
			if (formResettingComponent != null) {
				formResettingComponent.resetForm();
			}
		}
	}
}
