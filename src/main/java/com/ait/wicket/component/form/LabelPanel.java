package com.ait.wicket.component.form;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;

import com.ait.wicket.behavior.grid.BootstrapColumn;
import com.ait.wicket.behavior.grid.BootstrapGridLayoutBehavior;
import com.ait.wicket.behavior.grid.BootstrapLayout;
import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.component.grid.Bootstrap2GridLayoutPage;
import com.ait.wicket.component.grid.Bootstrap3GridLayoutPage;
import com.ait.wicket.component.i18n.I18nLabel;
import com.ait.wicket.util.LabelUtils;
import com.ait.wicket.util.WicketUtils;

/**
 * <p>
 * This {@link Border} is intended to easily create a {@link Label}, a
 * {@link FormComponent}, a {@link FeedbackPanel} and input hints if wanted.
 * </p>
 * <p>
 * The convention is: The wicketId of the {@link LabeledPanel} determines the
 * message keys of the {@link Label} and the input hint.
 * </p>
 * <p>
 * For example, if the associated markup in a page / panel looked like this:
 *
 * <pre>
 * {@code
 * <html>
 * 	<body>
 * 		<wicket:extend>
 *  		 <span wicket:id="selectAccount">
 * 	  		 <select wicket:id="selection"></select>
 *   		 </span>
 *  		 <span wicket:id="amount">
 * 	  		 <select wicket:id="amountField"></select>
 *   		 </span>
 * 		</wicket:extend>
 * 	</body>
 * </html>
 * }
 * </pre>
 *
 * </p>
 * <p>
 * The Java code would look like this:
 *
 * <pre>
 * add(new LabeledFeedbackBorder(&quot;selectAccount&quot;, new DropDownChoice&lt;AccountDto&gt;(&quot;selection&quot;, new Model(),
 * 		new ArrayList&lt;AccountDto&gt;(getAccountList()), new ChoiceRenderer&lt;AccountDto&gt;(&quot;iban&quot;))));
 * add(new LabeledFeedbackBorder(&quot;amount&quot;, new TextField&lt;BigDecimal&gt;(
 * 		&quot;amountField&quot;, new Model()))));
 * </pre>
 *
 * </p>
 * <p>
 * And the .properties file would look like:
 *
 * <pre>
 * selection.null=Choose...
 * selectAccount.label=Choose an account
 * amount.label=Choose an amount
 * amountField.hint=Format: #,###.##
 *
 * </pre>
 *
 * </p>
 *
 * @author Manuel
 */
public class LabelPanel extends Panel {
	private static final long serialVersionUID = 1L;

	private WebMarkupContainer label;
	private final String labelLookupId;
	private WebMarkupContainer requiredIndicator;
	private final BootstrapLayout labelGridLayout;

	private final Component input;

	private boolean required;

	public LabelPanel(final String id) {
		this(id, null);
	}

	public LabelPanel(final String id, final String labelLookupId) {
		this(id, labelLookupId, new BootstrapColumn(Grid.GRID_3));
	}

	public LabelPanel(final String id, final String labelLookupId,
			final BootstrapLayout labelGridLayout) {
		this(id, labelLookupId, labelGridLayout, null);
	}

	public LabelPanel(final String id, final String labelLookupId,
			final BootstrapLayout labelGridLayout, final Component input) {
		super(id);
		this.labelGridLayout = labelGridLayout;
		this.labelLookupId = labelLookupId;
		this.input = input;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		// adding label
		label = new WebMarkupContainer("label");
		add(label);

		if (input != null) {
			WicketUtils.addFor(input, new WebMarkupContainer("label"));
		}
		label.add(new I18nLabel("internalLabel", getLabelId()));

		// adding required behavior to label
		requiredIndicator = new WebMarkupContainer("requiredIndicator");
		label.add(requiredIndicator);

		if (getPage() instanceof Bootstrap2GridLayoutPage
				|| getPage() instanceof Bootstrap3GridLayoutPage) {

			if (labelGridLayout != null) {
				label.add(BootstrapGridLayoutBehavior.of(labelGridLayout));
			}
		}
	}

	private String getLabelId() {
		if (labelLookupId != null) {
			return labelLookupId;
		}
		return LabelUtils.getLabelIdForBorder(getId());
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		requiredIndicator.setVisible(isRequired());
	}

	public void addLabelBehavior(final Behavior... behaviors) {
		label.add(behaviors);
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(final boolean required) {
		this.required = required;
	}

}
