package com.ait.wicket.component.form.datepicker;

import java.util.Locale;

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.model.IModel;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ModelPatternDateConverter extends DateConverter {

	private IModel<String> datePattern;

	public ModelPatternDateConverter(IModel<String> datePattern,
			boolean applyTimeZoneDifference) {
		super(applyTimeZoneDifference);
		if (datePattern == null) {
			throw new IllegalArgumentException("datePattern must be not null");
		}
		this.datePattern = datePattern;
	}

	@Override
	public final String getDatePattern(Locale locale) {
		return datePattern.getObject();
	}
	
	@Override
	protected DateTimeFormatter getFormat(Locale locale) {
		return DateTimeFormat.forPattern(getDatePattern(locale))
				.withLocale(locale).withPivotYear(2000);
	}
}
