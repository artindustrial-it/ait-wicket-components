package com.ait.wicket.component.form.datepicker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class ConversionHelper {

	public static String toJson(String name, Serializable value) {
		return toJson(name, value, null);
	}

	public static String toJson(String name, Serializable value,
			String separator) {
		if (name != null && value != null) {
			return name + ": " + convertValue(value)
					+ (separator != null ? separator : "");
		} else {
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public static String convertValue(Serializable value) {
		StringBuilder sb = new StringBuilder();
		if (value instanceof Boolean) {
			sb.append(Boolean.TRUE.equals(value));
		} else if (value instanceof Integer) {
			sb.append(value);
		} else if (value instanceof Float) {
			sb.append(value);
		} else if (value instanceof Collection) {
			sb.append("[");
			Collection<Serializable> coll = (Collection<Serializable>) value;
			if (!coll.isEmpty()) {
				for (Iterator<Serializable> it = coll.iterator(); it
						.hasNext();) {
					Serializable next = it.next();
					if (next != null) {
						sb.append(convertValue(next)).append(",");
					}
				}
				if (sb.length() > 1) {
					sb.replace(sb.length() - 1, sb.length(), "");
				}
			}
			sb.append("]");
		} else if (value instanceof String[]) {
			return convertValue(
					new ArrayList<String>(Arrays.asList((String[]) value)));
		} else {
			sb.append("\"").append(value.toString()).append("\"");
		}
		return sb.toString();
	}
}
