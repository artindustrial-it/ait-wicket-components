package com.ait.wicket.component.form;


public interface ServiceContributor {

	/**
	 * This method calls the service and gives back an return code if any problem did happen. (Must not throw an exception)
	 * 
	 * The default message is from {@link ServiceCallingSubmitLink#DEFAULT_MESSAGE_KEY}
	 * 
	 * @return false if the ServiceCall did not complete successfully
	 */
	public boolean doServiceCallOnSave();
}
