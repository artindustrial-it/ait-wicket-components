package com.ait.wicket.component.form;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

public class BootstrapFeedbackPanel extends FeedbackPanel {

	/**
	 * @see org.apache.wicket.Component#Component(String)
	 */
	public BootstrapFeedbackPanel(final String id) {
		this(id, null);
	}

	/**
	 * @see org.apache.wicket.Component#Component(String)
	 *
	 * @param id
	 * @param filter
	 */
	public BootstrapFeedbackPanel(final String id,
			final IFeedbackMessageFilter filter) {
		super(id, filter);
	}

	@Override
	protected String getCSSClass(final FeedbackMessage message) {
		String result = "";
		switch (message.getLevel()) {
		case FeedbackMessage.INFO:
			result = "text-info";
			break;
		case FeedbackMessage.SUCCESS:
			result = "text-success";
			break;
		case FeedbackMessage.WARNING:
			result = "text-warning";
			break;
		case FeedbackMessage.ERROR:
		case FeedbackMessage.FATAL:
			result = "text-error";
			break;
		case FeedbackMessage.DEBUG:
		case FeedbackMessage.UNDEFINED:
			result = "muted";
			break;
		}
		return result;
	}

}
