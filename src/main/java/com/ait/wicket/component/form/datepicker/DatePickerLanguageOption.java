package com.ait.wicket.component.form.datepicker;

import java.io.Serializable;

public class DatePickerLanguageOption implements Serializable {

	private String languageName;
	private String[] days;
	private String[] daysShort;
	private String[] daysMin;
	private String[] months;
	private String[] monthsShort;
	private String today;
	private String clear;
	private String titleFormat;
	private int weekStart;

	public DatePickerLanguageOption(String languageName, String[] days,
			String[] daysShort, String[] daysMin, String[] months,
			String[] monthsShort, String today, String clear,
			String titleFormat, int weekStart) {
		this.languageName = languageName;
		this.days = days;
		this.daysShort = daysShort;
		this.daysMin = daysMin;
		this.months = months;
		this.monthsShort = monthsShort;
		this.today = today;
		this.clear = clear;
		this.titleFormat = titleFormat;
		this.weekStart = weekStart;
	}

	public String getLanguageName() {
		return languageName;
	}

	public String[] getDays() {
		return days;
	}

	public String[] getDaysShort() {
		return daysShort;
	}

	public String[] getDaysMin() {
		return daysMin;
	}

	public String[] getMonths() {
		return months;
	}

	public String[] getMonthsShort() {
		return monthsShort;
	}

	public String getToday() {
		return today;
	}

	public String getClear() {
		return clear;
	}

	public String getTitleFormat() {
		return titleFormat;
	}

	public int getWeekStart() {
		return weekStart;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public void setDays(String[] days) {
		this.days = days;
	}

	public void setDaysShort(String[] daysShort) {
		this.daysShort = daysShort;
	}

	public void setDaysMin(String[] daysMin) {
		this.daysMin = daysMin;
	}

	public void setMonths(String[] months) {
		this.months = months;
	}

	public void setMonthsShort(String[] monthsShort) {
		this.monthsShort = monthsShort;
	}

	public void setToday(String today) {
		this.today = today;
	}

	public void setClear(String clear) {
		this.clear = clear;
	}

	public void setTitleFormat(String titleFormat) {
		this.titleFormat = titleFormat;
	}

	public void setWeekStart(int weekStart) {
		this.weekStart = weekStart;
	}

	public String getValuesAsString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(ConversionHelper.toJson("days", days, ","));
		sb.append(ConversionHelper.toJson("daysShort", daysShort, ","));
		sb.append(ConversionHelper.toJson("daysMin", daysMin, ","));
		sb.append(ConversionHelper.toJson("months", months, ","));
		sb.append(ConversionHelper.toJson("monthsShort", monthsShort, ","));
		sb.append(ConversionHelper.toJson("today", today, ","));
		sb.append(ConversionHelper.toJson("clear", clear, ","));
		sb.append(ConversionHelper.toJson("titleFormat", titleFormat, ","));
		// has to be the last item because of the lacking separator
		sb.append(ConversionHelper.toJson("weekStart", weekStart));
		sb.append("}");
		return sb.toString();
	}

	// @formatter:off
	public static DatePickerLanguageOption DE = 
			new DatePickerLanguageOption(
				"de", 
				new String[] {"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"}, 
				new String[] {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam"},
				new String[] {"So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"},
				new String[] {"Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"},
				new String[] {"Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"},
			    "Heute",
			    "Löschen",
			    "MM yyyy",
			    1
			);
	// @formatter:on
}
