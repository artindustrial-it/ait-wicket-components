package com.ait.wicket.component.form;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.feedback.ContainerFeedbackMessageFilter;
import org.apache.wicket.feedback.IFeedback;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import com.ait.wicket.behavior.CssClassAppendingBehavior;
import com.ait.wicket.behavior.InputHintBehavior;
import com.ait.wicket.behavior.grid.BootstrapColumn;
import com.ait.wicket.behavior.grid.BootstrapGridLayoutBehavior;
import com.ait.wicket.behavior.grid.BootstrapLayout;
import com.ait.wicket.behavior.grid.BootstrapLayout.Grid;
import com.ait.wicket.behavior.grid.BootstrapRow;
import com.ait.wicket.behavior.grid.CssClassLayout;
import com.ait.wicket.component.grid.Bootstrap2GridLayoutPage;
import com.ait.wicket.component.grid.Bootstrap3GridLayoutPage;
import com.ait.wicket.util.LabelUtils;

/**
 * <p>
 * This {@link Border} is intended to easily create a {@link Label}, a
 * {@link FormComponent}, a {@link FeedbackPanel} and input hints if wanted.
 * </p>
 * <p>
 * The convention is: The wicketId of the {@link LabeledFeedbackBorder}
 * determines the message keys of the {@link Label} and the input hint.
 * </p>
 * <p>
 * For example, if the associated markup in a page / panel looked like this:
 *
 * <pre>
 *   &lt;html&gt;
 *   &lt;body&gt;
 *     &lt;wicket:extend&gt;
 *  		 &lt;span wicket:id="selectAccount"&gt;
 * 	  		 &lt;select wicket:id="selection"&gt;&lt;/select&gt;
 *   		 &lt;/span&gt;
 *  		 &lt;span wicket:id="amount"&gt;
 * 	  		 &lt;select wicket:id="amountField"&gt;&lt;/select&gt;
 *   		 &lt;/span&gt;
 *     &lt;/wicket:extend&gt;
 *   &lt;/body&gt;
 *   &lt;/html&gt;
 *
 *
 * </pre>
 *
 * </p>
 * <p>
 * The Java code would look like this:
 *
 * <pre>
 * add(new LabeledFeedbackBorder(&quot;selectAccount&quot;, new DropDownChoice&lt;AccountDto&gt;(&quot;selection&quot;, new Model(),
 * 		new ArrayList&lt;AccountDto&gt;(getAccountList()), new ChoiceRenderer&lt;AccountDto&gt;(&quot;iban&quot;))));
 * add(new LabeledFeedbackBorder(&quot;amount&quot;, new TextField&lt;BigDecimal&gt;(
 * 		&quot;amountField&quot;, new Model()))));
 * </pre>
 *
 * </p>
 * <p>
 * And the .properties file would look like:
 *
 * <pre>
 * selection.null=Choose...
 * selectAccount.label=Choose an account
 * amount.label=Choose an amount
 * amountField.hint=Format: X,XXX.XX
 *
 * </pre>
 *
 * </p>
 *
 * @author Manuel
 */
public class LabeledFeedbackBorder extends Border implements IFeedback {

	private static final long serialVersionUID = 1L;
	private LabelPanel beforeLabel;
	private LabelPanel afterLabel;
	// private WebMarkupContainer label;
	private final String labelLookupId;
	private final BootstrapLayout borderGridLayout;
	private final BootstrapLayout labelGridLayout;
	private final BootstrapLayout feedbackGridLayout;
	private final Component[] childs;
	private boolean labelAfterInput = false;

	public LabeledFeedbackBorder(final String id, final Component... childs) {
		this(id, null, childs);
	}

	public LabeledFeedbackBorder(final String id,
			final BootstrapLayout borderGridLayout,
			final BootstrapLayout labelGridLayout,
			final BootstrapLayout feedbackGridLayout, final Component... childs) {
		this(id, null, borderGridLayout, labelGridLayout, feedbackGridLayout, childs);
	}

	public LabeledFeedbackBorder(final String id, final String labelLookupId,
			final Component... childs) {
		this(id, labelLookupId, new BootstrapRow(), new BootstrapColumn(
				Grid.GRID_3), new BootstrapColumn(Grid.GRID_3), childs);
	}

	public LabeledFeedbackBorder(final String id, final String labelLookupId,
			final BootstrapLayout borderGridLayout,
			final BootstrapLayout labelGridLayout,
			final BootstrapLayout feedbackGridLayout, final Component... childs) {
		super(id);
		if (childs == null) {
			throw new IllegalArgumentException(
					"There have to be at least on child element in "
							+ this.getClass().getSimpleName());
		}
		this.borderGridLayout = borderGridLayout;
		this.labelGridLayout = labelGridLayout;
		this.feedbackGridLayout = feedbackGridLayout;
		this.labelLookupId = labelLookupId;
		this.childs = childs;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		// adding label
		beforeLabel = new LabelPanel("label.before", getLabelId(),
				labelGridLayout);
		addToBorder(beforeLabel);
		afterLabel = new LabelPanel("label.after", getLabelId(),
				labelGridLayout);
		addToBorder(afterLabel);

		// adding child components
		add(childs);
		childs[0].setOutputMarkupId(true);
		for (final Component item : childs) {
			final String text = Application.get().getResourceSettings()
					.getLocalizer()
					.getString(item.getId() + ".hint", this, null, "");
			if (text != null && text.length() > 0) {
				item.add(new InputHintBehavior(this.findParent(Form.class),
						text, "color: #aaa;"));
			}
		}

		// adding feedback panel
		// final Component feedbackPanel = new FeedbackPanel("feedback",
		// getMessagesFilter());
		final Component feedbackPanel = new FeedbackPanel("feedback",
				getMessagesFilter()).add(new CssClassAppendingBehavior(Model
				.of(CssClassLayout.RED)));
		addToBorder(feedbackPanel);

		if (getPage() instanceof Bootstrap2GridLayoutPage
				|| getPage() instanceof Bootstrap3GridLayoutPage) {
			childs[0].add(BootstrapGridLayoutBehavior.of(labelGridLayout));
			if (borderGridLayout != null) {
				this.add(BootstrapGridLayoutBehavior.of(borderGridLayout));
			}
			if (feedbackGridLayout != null) {
				feedbackPanel.add(BootstrapGridLayoutBehavior
						.of(feedbackGridLayout));
			}
		}
	}

	private String getLabelId() {
		if (labelLookupId != null) {
			return labelLookupId;
		}
		return LabelUtils.getLabelIdForBorder(getId());
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		beforeLabel.setVisible(!isLabelAfterInput());
		afterLabel.setVisible(isLabelAfterInput());
		beforeLabel.setRequired(isAnyInputRequired());
		afterLabel.setRequired(isAnyInputRequired());
	}

	private boolean isLabelAfterInput() {
		return labelAfterInput;
	}

	public LabeledFeedbackBorder setLabelAfterInput(
			final boolean labelAfterInput) {
		this.labelAfterInput = labelAfterInput;
		return this;
	}

	private boolean isAnyInputRequired() {
		for (final Component component : childs) {
			if (component instanceof FormComponent<?>
					&& ((FormComponent<?>) component).isRequired()) {
				return true;
			}
		}
		return false;
	}

	public void addLabelBehavior(final Behavior... behaviors) {
		beforeLabel.add(behaviors);
		afterLabel.add(behaviors);
	}

	/**
	 * @return Let subclass specify some other filter
	 */
	protected IFeedbackMessageFilter getMessagesFilter() {
		return new ContainerFeedbackMessageFilter(this);
	}
}
