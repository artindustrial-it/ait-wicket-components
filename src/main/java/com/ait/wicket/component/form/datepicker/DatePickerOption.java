package com.ait.wicket.component.form.datepicker;

import java.io.Serializable;
import java.util.ArrayList;

public class DatePickerOption<T extends Serializable> implements Serializable {
	private String name;
	private T value;

	private DatePickerOption(String name, T value) {
		this.name = name;
		this.value = value;
	}
	
	public static DatePickerOption<ArrayList<? extends Serializable>> getArrayOption(String name) {
		return getArrayOption(name, new ArrayList<Serializable>());
	}
	
	public static DatePickerOption<Boolean> getBooleanOption(String name) {
		return getBooleanOption(name, null);
	}
	
	public static DatePickerOption<String> getStringOption(String name) {
		return getStringOption(name, null);
	}
	
	public static DatePickerOption<Integer> getIntegerOption(String name) {
		return getIntegerOption(name, null);
	}
	
	public static DatePickerOption<Float> getFloatOption(String name) {
		return getFloatOption(name, null);
	}

	public static DatePickerOption<ArrayList<? extends Serializable>> getArrayOption(String name, ArrayList<Serializable> value) {
		return new DatePickerOption<ArrayList<? extends Serializable>>(name, value);
	}
	
	public static DatePickerOption<Boolean> getBooleanOption(String name, Boolean value) {
		return new DatePickerOption<Boolean>(name, value);
	}
	
	public static DatePickerOption<String> getStringOption(String name, String value) {
		return new DatePickerOption<String>(name, value);
	}
	
	public static DatePickerOption<Integer> getIntegerOption(String name, Integer value) {
		return new DatePickerOption<Integer>(name, value);
	}
	
	public static DatePickerOption<Float> getFloatOption(String name, Float value) {
		return new DatePickerOption<Float>(name, value);
	}
	
	public String getName() {
		return name;
	}

	public T getValue() {
		return value;
	}
}
