package com.ait.wicket.component.form.datepicker;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import org.apache.wicket.Page;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.util.convert.IConverter;

import com.ait.wicket.component.grid.Bootstrap2GridLayoutPage;
import com.ait.wicket.component.grid.Bootstrap3GridLayoutPage;

/**
 * The Bootstrap <a href="http://www.eyecon.ro/bootstrap-datepicker/">DatePicker</a> as
 * Wicket component.
 *
 * The component can be configured to use a specific date pattern. See
 * {@link java.text.SimpleDateFormat} for available patterns.
 *
 * PLEASE NOTE: NOT FINAL!
 * THIS IS AN EARLY PREVIEW
 *
 * @author <a href="mailto:code@benjaminplocek.com">Benjamin Plocek</a>
 */
@SuppressWarnings("unchecked")
public class BootstrapDatePicker extends TextField<Date> {

	private static final String FORMAT = "format";
	private static final String BOOTSTRAP2_JS_URL = "//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js";
	private static final JavaScriptHeaderItem BOOTSTRAP2_JS = JavaScriptHeaderItem.forUrl(BOOTSTRAP2_JS_URL);
	private static final String BOOTSTRAP3_JS_URL = "//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js";
	private static final JavaScriptHeaderItem BOOTSTRAP3_JS = JavaScriptHeaderItem.forUrl(BOOTSTRAP3_JS_URL);
	private static final JavaScriptResourceReference DATEPICKER_JS_REF = new JavaScriptResourceReference(BootstrapDatePicker.class, "BootstrapDatePicker.js");
	public static final JavaScriptHeaderItem DATEPICKER_JS = JavaScriptHeaderItem.forReference(DATEPICKER_JS_REF);
	private static final ResourceReference DATEPICKER_CSS_REF = new PackageResourceReference(BootstrapDatePicker.class, "BootstrapDatePicker.css");
	public static final CssHeaderItem DATEPICKER_CSS = CssHeaderItem.forReference(DATEPICKER_CSS_REF);

	public static enum WeekStart {
		SUNDAY(0), MONDAY(1), TUESDAY(2),
		WEDNESDAY(3), THURSDAY(4), FRIDAY(5),
		SATURDAY(6);
		private final int value;

		private WeekStart(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	/**
	 * The default date pattern: yyyy-mm-dd
	 */
	public static final String DEFAULT_DATE_FORMAT = "yyyy-mm-dd";
	/**
	 * The default week start: {@link #SUNDAY}
	 */
	public static final transient WeekStart DEFAULT_WEEK_START = WeekStart.SUNDAY;

	private IModel<String> dateFormatModel;
	private IConverter<Date> converter;
	private boolean loadBootstrapJavascript = true;
	private boolean loadCss = true;
	private HashMap<String, DatePickerOption<?>> optionSettings;
	private HashMap<String, DatePickerLanguageOption> languageSettings;

	/**
	 * Creates a date picker with {@link #DEFAULT_DATE_FORMAT} as date pattern and
	 * {@link #DEFAULT_WEEK_START} as first day of week
	 *
	 * @param id
	 */
	public BootstrapDatePicker(String id) {
		this(id, DEFAULT_DATE_FORMAT, DEFAULT_WEEK_START);
	}

	/**
	 * Creates a date picker with {@link #DEFAULT_WEEK_START} as first day of week.
	 *
	 * @param id             The wicket id.
	 * @param dateFormat     The date format in {@link java.text.SimpleDateFormat} pattern.
	 *                       This will be automatically converted for the datepicker.js.
	 */
	public BootstrapDatePicker(String id, String dateFormat) {
		this(id, dateFormat, DEFAULT_WEEK_START);
	}

	/**
	 * Creates a date picker with {@link #DEFAULT_DATE_FORMAT} as date pattern.
	 *
	 * @param id             The wicket id.
	 * @param firstDayOfWeek The first day of week, {@link #SUNDAY} or {@link #MONDAY}.
	 */
	public BootstrapDatePicker(String id, WeekStart firstDayOfWeek) {
		this(id, DEFAULT_DATE_FORMAT, firstDayOfWeek);
	}

	/**
	 * Creates a date picker with the given date format and first day of week.
	 *
	 * @param id             The wicket id.
	 * @param dateFormat     The date format in {@link java.text.SimpleDateFormat} pattern.
	 *                       This will be automatically converted for the datepicker.js.
	 * @param firstDayOfWeek The first day of week, {@link #SUNDAY} or {@link #MONDAY}.
	 */
	public BootstrapDatePicker(String id, String dateFormat, WeekStart firstDayOfWeek) {
		this(id, dateFormat, firstDayOfWeek, null);
	}

	/**
	 * @param id             The wicket id.
	 * @param dateFormat     The date format in {@link java.text.SimpleDateFormat} pattern.
	 *                       This will be automatically converted for the datepicker.js.
	 * @param firstDayOfWeek The first day of week, {@link WeekStart#SUNDAY} or {@link WeekStart#MONDAY} or ...
	 * @param converter      The converter to use.
	 */
	public BootstrapDatePicker(String id, String dateFormat, WeekStart firstDayOfWeek, IConverter<Date> converter) {
		super(id);
		setOutputMarkupId(true);
		this.languageSettings = new HashMap<String, DatePickerLanguageOption>();
		this.optionSettings = new HashMap<String, DatePickerOption<?>>();
		this.dateFormatModel = new Model<String>();
		// this initializes the dateModel 
		setDateFormat(dateFormat);
		setWeekStart(firstDayOfWeek);
		this.converter = converter;
		if (this.converter == null) {
			this.converter = new ModelPatternDateConverter(dateFormatModel, false);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public IConverter<Date> getConverter(Class type) {
		return converter;
	}

	public void setLanguageSettings(DatePickerLanguageOption... options) {
		if (options != null) {
			for (DatePickerLanguageOption item : options) {
				languageSettings.put(item.getLanguageName(), item);
			}
		}
	}
	
	public void clearOptionSettings(DatePickerOption<?>... options) {
		optionSettings.clear();
	}

	public void setOptionSettings(DatePickerOption<?>... options) {
		if (options != null) {
			for (DatePickerOption<? extends Serializable> item : options) {
				optionSettings.put(item.getName(), item);
				if (FORMAT.equals(item.getName())) {
					_setDateFormat((String) item.getValue());
				}
			}
		}
	}
	
	@Override
	public void renderHead(IHeaderResponse response) {
		// Bootstrap
		Page page = getPage();
		if (isLoadBootstrapJavascript()) {
			if (page instanceof Bootstrap2GridLayoutPage) {
				response.render(BOOTSTRAP2_JS);
			} else if (page instanceof Bootstrap3GridLayoutPage) {
				response.render(BOOTSTRAP3_JS);
			}

			// Datepicker
			response.render(DATEPICKER_JS);
		}
		if (isLoadCss()) {
			response.render(DATEPICKER_CSS);
		}

		// Configuration
		String js = getJsString();
		response.render(new OnDomReadyHeaderItem(js));
	}

	private String getJsString() {
		StringBuilder sb = new StringBuilder();
		if (!languageSettings.isEmpty()) {
			sb.append(getLanguageStringFromSettings());
		}
		sb.append(getOptionsStringFromSettings());
		return sb.toString();
	}

	private String getOptionsStringFromSettings() {
		StringBuilder sb = new StringBuilder();
		sb.append("jQuery(\"#");
		sb.append(getMarkupId());
		sb.append("\").datepicker(");
		sb.append(getStringFromDatePickerOptionArray(optionSettings.values()));
		sb.append(");\n");
		return sb.toString(); 
	}
	
	public BootstrapDatePicker setLanguage(String value) {
		setOptionSettings(DatePickerOption.getStringOption("language", value));
		return this;
	}

	protected void _setDateFormat(String value) {
		dateFormatModel.setObject(convertToJavaDateFormat(value));
	}
	
	public BootstrapDatePicker setDateFormat(String value) {
		setOptionSettings(DatePickerOption.getStringOption(FORMAT, value));
		return this;
	}
	
	public BootstrapDatePicker setWeekStart(WeekStart value) {
		setOptionSettings(DatePickerOption.getIntegerOption("weekStart", value.getValue()));
		return this;
	}

	public BootstrapDatePicker setAutoclose(Boolean value) {
		setOptionSettings(DatePickerOption.getBooleanOption("autoclose", value));
		return this;
	}

	/**
	 * Converts the {@link #dateFormat} to the date format Java expects.
	 *
	 * @return The date pattern for java.
	 */
	protected String convertToJavaDateFormat(String dateFormat) {
		if (dateFormat.contains("mm")) {
			return dateFormat.replace("mm", "MM");
		}
		return dateFormat;
	}

	private String getStringFromDatePickerOptionArray(Collection<DatePickerOption<?>> list) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for (DatePickerOption<?> item : list) {
			String name = item.getName();
			Serializable value = item.getValue();
			sb.append(ConversionHelper.toJson(name, value, ","));
		}
		if (sb.length() > 1) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("}");
		return sb.toString();
	}

	private String getLanguageStringFromSettings() {
		StringBuilder sb = new StringBuilder();
		if (!languageSettings.isEmpty()) {
			for (DatePickerLanguageOption entry : languageSettings.values()) {
				sb.append("jQuery.fn.datepicker.dates[\"").append(entry.getLanguageName()).append("\"] = ");
				sb.append(entry.getValuesAsString());
				sb.append(";\n");
			}
		}
		return sb.toString();
	}

	public boolean isLoadBootstrapJavascript() {
		return loadBootstrapJavascript;
	}

	public BootstrapDatePicker setLoadBootstrapJavascript(boolean loadBootstrapJavascript) {
		this.loadBootstrapJavascript = loadBootstrapJavascript;
		return this;
	}

	public boolean isLoadCss() {
		return loadCss;
	}

	public BootstrapDatePicker setLoadCss(boolean loadCss) {
		this.loadCss = loadCss;
		return this;
	}
}
