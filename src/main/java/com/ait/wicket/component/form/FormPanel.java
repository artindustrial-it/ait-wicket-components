package com.ait.wicket.component.form;

import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import com.ait.wicket.component.generics.TypedPanel;

/**
 * @author Manuel
 */
public abstract class FormPanel<T> extends TypedPanel<T> implements FormResettingComponent, ServiceContributor {
	private static final long	serialVersionUID = 1L;

	public FormPanel(final String id) {
		super(id);
		resetForm();
	}

	public final void resetForm() {
		if (getModel() == null) {
			setDefaultModel(new CompoundPropertyModel<T>(newFormObject()));
		} else {
			setModelObject(newFormObject());
		}
	}

	/**
	 * Must be implemented<br/>
	 * Creates a new Object needed for forms.
	 * 
	 * @return a new instance of T
	 */
	protected abstract T newFormObject();

	public FormPanel(final String id, final IModel<? extends T> model) {
		super(id, model);
	}
}
