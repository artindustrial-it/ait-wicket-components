package com.ait.wicket.component.form;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;

import com.ait.wicket.component.generics.TypedPanel;

/**
 * @author Manuel
 */
public class ProgressPanel extends TypedPanel<Integer> {
	private static final long serialVersionUID = 1L;

	public ProgressPanel(final String id, IModel<Integer> progress) {
		super(id, progress);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		// adding label
		Label label = new Label("label.progress", new PrefixSuffixModel(getModel(),
				"", " %"));
		label.add(new AttributeModifier("style", new PrefixSuffixModel(getModel(),
				"width: ", "%", "0")));
		setOutputMarkupId(true);
		add(label);

		// update progressbar every second via ajax
		add(new AbstractAjaxTimerBehavior(Duration.seconds(1)) {
			@Override
			protected void onTimer(AjaxRequestTarget target) {
				onUpdateProgress(target);
			}
		});
	}

	/**
	 * Override me, if you want to do custom stuff when the {@link ProgressPanel}
	 * updates itself.<br>
	 * <br>
	 * e.g. You could update the whole {@link Page} when the progress is at 100.
	 * 
	 * @param target
	 */
	protected void onUpdateProgress(AjaxRequestTarget target) {
		target.add(ProgressPanel.this);
	}

	private static class PrefixSuffixModel extends Model<String> {
		private IModel<?> innerModel;
		private String suffix;
		private String prefix;
		private String defaultValue;

		public PrefixSuffixModel(IModel<?> innerModel, String prefix, String suffix) {
			this(innerModel, prefix, suffix, null);
		}

		public PrefixSuffixModel(IModel<?> innerModel, String prefix,
				String suffix, String defaultValue) {
			this.innerModel = innerModel;
			this.prefix = prefix;
			this.suffix = suffix;
			this.defaultValue = defaultValue;
		}

		@Override
		public String getObject() {
			String object = getObjectOrDefault();
			if (object != null) {
				return prefix + object + suffix;
			}
			return "";
		}

		private String getObjectOrDefault() {
			if (innerModel != null && innerModel.getObject() != null) {
				return "" + innerModel.getObject();
			} else if (defaultValue != null) {
				return defaultValue;
			} else {
				return null;
			}
		}
	}
}
