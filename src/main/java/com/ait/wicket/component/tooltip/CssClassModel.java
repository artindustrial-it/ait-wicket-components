package com.ait.wicket.component.tooltip;

import org.apache.wicket.model.PropertyModel;

public class CssClassModel extends PropertyModel<String> {

	private String additionalClass;

	public CssClassModel(Object modelObject, String expression) {
		super(modelObject, expression);
	}

	public CssClassModel(Object modelObject, String expression, String additionalClass) {
		super(modelObject, expression);
		this.additionalClass = additionalClass;
	}

	@Override
	public String getObject() {
		String result = super.getObject();
		if (additionalClass != null && additionalClass.length() > 0) {
			result += " " + additionalClass;
		}
		return result;
	}
}
