package com.ait.wicket.component.tooltip;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.ChainingModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.ait.wicket.component.generics.TypedPanel;

public class IconPanel extends TypedPanel<String> {

	private IModel<IconSettings> settingsModel;
	private WebMarkupContainer link;
	private WebMarkupContainer icon;
	private boolean renderWicketLink;
	private Label tooltipInitJs;
	private IModel<String> titleModel;
	private IModel<String> additionalJsModel;

	/**
	 * @param id
	 *            the wicket id for this component
	 * @param titleModel
	 *            the popover title
	 * @param contentModel
	 *            the popover content
	 */
	public IconPanel(String id, IModel<String> titleModel,
			IModel<String> contentModel) {
		this(id, titleModel, contentModel, Model.of(IconSettings.defaults()));
	}

	/**
	 * @param id
	 *            the wicket id for this component
	 * @param titleModel
	 *            the popover title
	 * @param contentModel
	 *            the popover content
	 * @param settingsModel
	 *            bootstrap popover settings
	 * @see http://getbootstrap.com/2.3.2/javascript.html#popovers
	 */
	public IconPanel(String id, IModel<String> titleModel,
			IModel<String> contentModel, IModel<IconSettings> settingsModel) {
		this(id, titleModel, null, contentModel, settingsModel);
	}

	/**
	 * 
	 * @param id
	 *            the wicket id for this component
	 * @param titleModel
	 *            the popover title
	 * @param additionalJsModel
	 *            can be used to execute additional Javascript after the popover
	 *            js has been processed (if you need the html element id, use
	 *            the variable elementId in your js string.
	 * @param contentModel
	 *            the popover content
	 * @param settingsModel
	 *            bootstrap popover settings
	 * @see http://getbootstrap.com/2.3.2/javascript.html#popovers
	 */
	public IconPanel(String id, IModel<String> titleModel,
			IModel<String> additionalJsModel, IModel<String> contentModel,
			IModel<IconSettings> settingsModel) {
		super(id, contentModel);
		this.titleModel = titleModel;
		this.additionalJsModel = additionalJsModel;
		this.settingsModel = settingsModel;
	}

	public IconPanel(String id, IModel<String> model, boolean renderWicketLink,
			IModel<IconSettings> settingsModel) {
		super(id, model);
		this.renderWicketLink = renderWicketLink;
		this.settingsModel = settingsModel;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		if (renderWicketLink) {
			link = new Link<Object>("link") {
				@Override
				public void onClick() {
					IconPanel.this.onClick();
				}
			};
		} else {
			link = new WebMarkupContainer("link");
		}
		this.add(link);
		link.add(new AttributeModifier("href",
				new PropertyModel<String>(settingsModel, "link")));
		link.add(new AttributeModifier("title", titleModel));
		link.add(new AttributeModifier("data-content", getModel()));
		final String nodeId = "Popover_" + getMarkupId();
		link.add(new AttributeModifier("id", nodeId));
		icon = new WebMarkupContainer("icon");
		link.add(icon);
		icon.add(new AttributeModifier("class",
				new CssClassModel(settingsModel, "iconClass", "tooltip-icon")));
		IModel<String> jsScriptModel = new ChainingModel<String>(
				new PropertyModel<String>(settingsModel, "tooltipOptions")) {
			@Override
			public String getObject() {
				String options = super.getObject();
				String additionalJs = additionalJsModel != null
						&& additionalJsModel.getObject() != null
								? additionalJsModel.getObject()
								: "";
				// @formatter:off
				return "document.addEventListener(\"DOMContentLoaded\", function(event) {"
						+ "var elementId = '" + nodeId + "';"
						+ "jQuery('#' + elementId).popover("
						+ (options != null ? options : "") 
						+ ");"
						+ additionalJs 
						+ "});";
				// @formatter:on
			}
		};
		tooltipInitJs = new Label("tooltipInitJs", jsScriptModel);
		add(tooltipInitJs);
		tooltipInitJs.setEscapeModelStrings(false);
	}

	protected void onClick() {
		throw new RuntimeException("If you render a wicket link in "
				+ this.getClass().getSimpleName()
				+ " you have to override this method");
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
	}
}
