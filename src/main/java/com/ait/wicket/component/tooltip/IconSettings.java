package com.ait.wicket.component.tooltip;

import java.io.Serializable;

public class IconSettings implements Serializable {
	private String link;
	private String iconClass;
	private String tooltipOptions;

	public IconSettings(String iconClass) {
		this(iconClass, null);
	}

	public IconSettings(String iconClass, String link) {
		this(iconClass, link, "");
	}

	public IconSettings(String iconClass, String link, String tooltipOptions) {
		this.iconClass = iconClass;
		this.link = link;
		this.tooltipOptions = tooltipOptions;
	}
	
	public static IconSettings defaults() {
		return new IconSettings("icon-question-sign icon-fixed-width");
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getTooltipOptions() {
		return tooltipOptions;
	}

	public void setTooltipOptions(String tooltipOptions) {
		this.tooltipOptions = tooltipOptions;
	}

}
