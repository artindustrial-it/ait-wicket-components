package com.ait.wicket.component.grid;

/**
 * Marker interface. If a page is an instance of {@link Bootstrap3GridLayoutPage} the
 * components in it may react in some way to it.
 * 
 * @author Manuel
 * 
 */
public interface Bootstrap3GridLayoutPage {
}
