ait-wicket-components
=====================

About:
------

This is a collection of all reusable (non business related) artindustrial it 
Wicket components. 
You can use it in your software project if you built on 
Apache Wicket (http://wicket.apache.org) as framework. 

License:
--------

This software project builds upon Apache Wicket 
and is licensed under the Apache Software Foundation license, version 2.0.

When should a component be added here:
--------------------------------------

Add only if your component... 
* is ready for production 
  (or javadoc that the component is not meant for production and why)
* adds value / saves writing boilerplate code again and again
* is reusable
* is general purpose
* does not contain any domain / business specific code / logic
* well documented so any developer can use it without troubles 
  or looking into the code for hours

Comments:
---------

To try / see the components in action see the project 'ait-wicket-components-examples'

Be careful when updating Flotr2 (js chart library) - we had to fix a problem that the 
legend will not be printed in IE when it is in an external container.
